package com.softwaremedida.springboot.backend.apirest.services;

import com.softwaremedida.springboot.backend.apirest.entity.Rol;

public interface IRolService {

	public abstract Rol findByNombre(String nombre);
	public abstract void save(Rol rol);
}
