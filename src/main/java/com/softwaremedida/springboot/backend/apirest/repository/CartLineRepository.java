package com.softwaremedida.springboot.backend.apirest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.CartLine;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
@Repository("cartLineRepository")
public interface CartLineRepository extends JpaRepository<CartLine, Long> {
	
	//@Query("from CartLine c where c.producto = ?1")
	@Query(value="SELECT * FROM linea_carro WHERE producto_id = ?1 ORDER BY created_at DESC",nativeQuery = true)
	public List<CartLine> findByProducto(Producto producto);
	
	
}
