package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="categoria")
public class Categoria extends AuditingModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@NotEmpty(message = "no puede estar vacio")
	@Column(unique = true,nullable = false)
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Categoria(@NotEmpty String nombre) {
		super();
		this.nombre = nombre;
	}
	
//	@OneToMany(mappedBy = "categoria")
//	private Set<SubCategoria> subCategoria;
//	
	public Categoria() {}
	
	

}
