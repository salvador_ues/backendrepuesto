package com.softwaremedida.springboot.backend.apirest.controllers;

import java.awt.PageAttributes.MediaType;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.Response;

import java.sql.Statement;

import org.hibernate.query.criteria.internal.expression.SearchedCaseExpression.WhenClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.ConsultasDB;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.RelacionColumna;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.models.ColumnasModel;
import com.softwaremedida.springboot.backend.apirest.models.ColumnasDBCentralModel;
import com.softwaremedida.springboot.backend.apirest.models.ColumnasSeleccionadasModel;
import com.softwaremedida.springboot.backend.apirest.models.ParametrosETLModel;
import com.softwaremedida.springboot.backend.apirest.models.ParametrosETLCSVModel;
import com.softwaremedida.springboot.backend.apirest.models.TablaModel;
import com.softwaremedida.springboot.backend.apirest.models.TablaColumnaModel;
import com.softwaremedida.springboot.backend.apirest.models.TablasModel;
import com.softwaremedida.springboot.backend.apirest.repository.AdminRepository;
import com.softwaremedida.springboot.backend.apirest.repository.SucursalRepository;
import com.softwaremedida.springboot.backend.apirest.services.ConexionServiceImp;
import com.softwaremedida.springboot.backend.apirest.services.IAdministradorService;
import com.softwaremedida.springboot.backend.apirest.services.IConexionService;
import com.softwaremedida.springboot.backend.apirest.services.IConsultaService;
import com.softwaremedida.springboot.backend.apirest.services.IProductoService;
import com.softwaremedida.springboot.backend.apirest.services.IRelacionColumnaService;
import com.softwaremedida.springboot.backend.apirest.services.ISubCategoriaService;
import com.softwaremedida.springboot.backend.apirest.services.ISucursalService;
import com.softwaremedida.springboot.backend.apirest.services.SucursalServiceImp;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api_db")
public class LoadDataBaseRestController {
	Conexion conexionGlobal = new Conexion();
	TablaModel tablaGlobal = new TablaModel();
	String gestorGlobal = "";

	@Autowired
	@Qualifier("conexionServiceImp")
	private ConexionServiceImp conexionService;

	@Autowired
	private IAdministradorService adminService;

	@Autowired
	private IProductoService productoService;

	@Autowired
	private ISubCategoriaService subCategoriaService;

	@Autowired
	private ISucursalService sucursalService;

	@Autowired
	private IAdministradorService administradorService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("consultaServiceImp")
	private IConsultaService consultaService;

	@Autowired
	private IRelacionColumnaService relacionService;

	private Sucursal userAuthenticate() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Sucursal sucursal = sucursalService
				.findByAdministrador(administradorService.findByUsuario(usuarioService.finByUsername(auth.getName())));
		return sucursal;
	}

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/parametros_db")
	public ResponseEntity<?> extraccionDatos(@RequestBody Conexion conexion) {
		Map<String, Object> respuesta = new HashMap<>();
		Conexion conexionR = null;

		try {
			System.out.println("POR AQUI ESTA PASANDO");

			conexion.setEstado(true);
			conexion.setSucursal(userAuthenticate());
			conexionR = conexionService.save(conexion);

			conexion.setConexion_id(conexionR.getConexion_id());
			conexionGlobal2(conexion);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar la conexión en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La consexión ha sido creada con exito!!");
		respuesta.put("conexion", conexionR);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	public void conexionGlobal2(Conexion conexion) {
		conexionGlobal.setConexion_id(conexion.getConexion_id());
		conexionGlobal.setGestor(conexion.getGestor());
		conexionGlobal.setNombre(conexion.getNombre());
		conexionGlobal.setHost(conexion.getHost());
		conexionGlobal.setPuerto(conexion.getPuerto());
		conexionGlobal.setUsuario(conexion.getUsuario());
		conexionGlobal.setPassword(conexion.getPassword());
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_conexiones")
	public List<Conexion> listConexiones() {

		return conexionService.findAll(userAuthenticate());
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_conexiones/page/{page}")
	public Page<Conexion> listConexiones(@PathVariable Integer page) {

		return conexionService.findAll(userAuthenticate(), PageRequest.of(page, 4));
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/show_conexion")
	public Conexion showConexion(@RequestBody Long id) {
		return conexionService.getConexion(id);
	}

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/tablas_seleccion")
	public String tablasSeleccion(@RequestBody TablaModel tablas) {

		System.out.println(tablas.getTabla().get(0));
		System.out.println("LISTA" + tablas.getTabla().listIterator());
		for (String tabla : tablas.getTabla()) {
			System.out.println("TODAS LAS TABLAS:" + tabla);
		}

		tablaGlobal.setTabla(tablas.getTabla());
		return null;
	}

	public Connection conexionBase() throws SQLException, ClassNotFoundException {

		String gestor = conexionGlobal.getGestor();
		String nombreDB = conexionGlobal.getNombre();
		String host = conexionGlobal.getHost();
		String puerto = conexionGlobal.getPuerto();
		String usuario = conexionGlobal.getUsuario();
		String password = conexionGlobal.getPassword();
		String urlGestor = "jdbc:" + gestor + "://" + host + ":" + puerto + "/" + nombreDB;
		if (gestor.equals("mariadb")) {
			Class.forName("org.mariadb.jdbc.Driver");
		}
		Connection DB = DriverManager.getConnection(urlGestor, usuario, password);

		return DB;
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/tablas_columnas")
	public String tablasColumnasDB() throws SQLException, ClassNotFoundException {

		Connection DB = conexionBase();
		// DatabaseMetaData metadatos;
		ResultSetMetaData resultmetadatos;
		Statement st = DB.createStatement();
		ResultSet rst;
		List<TablaColumnaModel> tablasColumnas = new ArrayList<TablaColumnaModel>();
		List<String> columnas = new ArrayList<String>();
		for (String tabla : tablaGlobal.getTabla()) {
			ResultSet rstable = st.executeQuery("select * from " + tabla);
			resultmetadatos = rstable.getMetaData();
			int col = resultmetadatos.getColumnCount();
			for (int i = 1; i <= col; i++) {
				columnas.add(tabla + " " + resultmetadatos.getColumnName(i));
			}
			tablasColumnas.add(new TablaColumnaModel(tabla, columnas));
		}
		String gson = new Gson().toJson(columnas);
		System.out.println("Resultado:" + columnas);
		DB.close();
		conexionBase().close();
		return gson;
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/parametros_db")
	public String tablasDB() throws SQLException, ClassNotFoundException {
		// Conexion con gestores
		Connection DB = null;
		String mensaje = "";
		// String mensaje2="";
		int cantidadTablas = 0;
		DatabaseMetaData metadatos;
		String tabla;
		String[] types = { "TABLE" };

		ResultSet rst = null;
		List<TablasModel> tablasObjetos = new ArrayList<TablasModel>();
		try {
			DB = conexionBase();
			boolean valid = DB.isValid(50000);
			System.out.println(valid ? "Conexion establecida" : "TEST FAIL");
			metadatos = DB.getMetaData();
			rst = metadatos.getTables(null, null, "%", types);

			while (rst.next()) {
				tabla = rst.getObject(3).toString();
				cantidadTablas += 1;
				tablasObjetos.add(new TablasModel(cantidadTablas, tabla));
			}

			System.out.println("Cantidad de tablas:" + String.valueOf(cantidadTablas));
			rst.close();

			mensaje = "conexion segura" + metadatos.getDatabaseProductName();

		} catch (java.sql.SQLException e) {
			mensaje = "conexion error";
			System.out.println("Error" + e);
		}

		String gson = new Gson().toJson(tablasObjetos);
		DB.close();
		conexionBase().close();
		return gson;
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/conexion_especifica/{id}")
	public Conexion conexionEspecifica(@PathVariable String id) {
		Conexion conexion = conexionService.getConexion(Long.parseLong(id));
		System.out.println("OBTENIENDO CONEXION:" + conexion);
		return conexion;
	}

	@Secured("ROLE_NEGOCIO")
	@PutMapping("/conexion_establecer/{id}")
	public Conexion conexionEstablecer(@RequestBody Conexion conexion, @PathVariable String id) {
		Conexion conexion2 = conexionService.getConexion(Long.parseLong(id));
		conexionGlobal2(conexion2);
		return null;
	}

	@Secured("ROLE_NEGOCIO")
	@PutMapping("/conexion_modificar/{id}")
	public ResponseEntity<?> conexionModificar(@RequestBody Conexion conexion, @PathVariable String id) {
		Conexion conexionActual = null, conexionModificada = null;
		Map<String, Object> respuesta = new HashMap<>();
		conexionActual = conexionService.getConexion(Long.parseLong(id));

		if (conexionActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {

			conexionActual.setGestor(conexion.getGestor());
			conexionActual.setNombre(conexion.getNombre());
			conexionActual.setHost(conexion.getHost());
			conexionActual.setPuerto(conexion.getPuerto());
			conexionActual.setUsuario(conexion.getUsuario());
			conexionActual.setPassword(conexion.getPassword());
			conexionModificada = conexionService.save(conexionActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La conexión ha sido actualizado con exito!!");
		respuesta.put("conexion", conexionModificada);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@DeleteMapping("/conexion_eliminar/{id}")
	public ResponseEntity<?> conexionEliminar(@PathVariable String id) {
		Map<String, Object> respuesta = new HashMap<>();
		Conexion conexionActual = null, conexionModificar = null;
		conexionActual = conexionService.getConexion(Long.parseLong(id));
		if (conexionActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		try {

			conexionActual.setEstado(false);
			conexionModificar = conexionService.save(conexionActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al eliminar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La conexión ha sido eliminada con exito!!");
		respuesta.put("comision", conexionModificar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/cargar_datos/save")
	public ResponseEntity<?> cargarDatosDB(@RequestBody ParametrosETLModel parametros)
			throws SQLException, ClassNotFoundException {
		int a = 1;
		Producto producto = null;
		Connection DB = null;
		DB = conexionBase();
		boolean valid = DB.isValid(50000);
		System.out.println(valid ? "Conexion establecida POST --salvando datos" : "TEST FAIL");

		Statement st = DB.createStatement();
		ResultSet rs = null;
		DatabaseMetaData metadatos;

		String queryConsulta = "", queryMetadata = "";
		List<String> tablaQuery = new ArrayList<String>();
		int contador = 1, contador2 = 0, contadorComparar = 0;
		ArrayList<String> consulta2 = new ArrayList<String>();
		String tablaColumna = "", tablaColumnaRelacion = "", tablaRepetida = "";
		ArrayList<String> tablas = new ArrayList<String>();
		List<String> relacionTablasPKFK = new ArrayList<String>();
		String gestor = conexionGlobal.getGestor();
		Long idConexion = conexionGlobal.getConexion_id();
		String consultaSubcategoria = "";
		// ConsultasDB consultaSubcategoria=null;
		for (String parametro : parametros.getColumasSeleccionadas()) {
			String[] parametroDB = parametro.split(" ");
			String tabla = parametroDB[0];
			String columna = parametroDB[1];

			if (gestor.equals("postgresql")) {
				columna = "\"" + columna + "\"";
			}

			if (parametros.getColumnas().getSubcategoria_nombre() == contador) {
				System.out.println("INDICE SUBNOMBRE:" + parametros.getColumnas().getSubcategoria_nombre());
				System.out.println("CONTADOR:" + contador);
				String consulta = "select " + tabla.concat(".").concat(columna) + " from " + tabla + ";";
				consultaSubcategoria = consulta;
				System.out.println("=====SUBCATEGORIA:" + consulta);
				rs = st.executeQuery(consulta);
				// System.out.println("RESULSET:" + rs);
				SubCategoria subCategoria, subCategoriaExiste = null;
				while (rs.next()) {
					subCategoriaExiste = subCategoriaService.findByNombreAndSucursal(rs.getString(1),
							userAuthenticate());
					if (subCategoriaExiste == null) {
						subCategoria = new SubCategoria();
						subCategoria.setNombre(rs.getString(1));
						subCategoria.setSucursal(userAuthenticate());
						subCategoria.setEstado(true);
						subCategoria = subCategoriaService.save(subCategoria);
					}

				}
				System.out.println("datos retornados:" + rs);
			}

			tablaColumnaRelacion = tabla.concat(".").concat(columna);
			consulta2.add(tablaColumnaRelacion);
			tablas.add(tabla);

			if (!tabla.equals(tablaRepetida)) {
				tablaQuery.add(tabla);
				tablaRepetida = tabla;
			}
			System.out.println("tabla:" + tabla);
			contador++;
		}

		for (String tabla2 : tablas) {
			contador2 = Collections.frequency(tablas, tabla2);
			System.out.println("contador de tablas:" + contador2);
			if (contador2 > contadorComparar) {
				contadorComparar = contador2;
				tablaColumna = tabla2;
			}
		}

		String tablaFk = "";
		for (String tq : tablaQuery) {
			if (!tq.equals(tablaColumna)) {
				tablaFk = tq;
			}
		}

		String queryGeneral = "select " + String.join(",", consulta2) + " from " + tablaColumna;

		String queryInnerjoin = "", queryTotal = "";

		if (gestor.equals("mysql")) {
			queryMetadata = "USE api_rest_laravel;\r\n" + "SELECT TABLE_NAME," + " COLUMN_NAME," + "CONSTRAINT_NAME,"
					+ " REFERENCED_TABLE_NAME " + "FROM KEY_COLUMN_USAGE " + "WHERE " + " TABLE_NAME = " + "\""
					+ tablaColumna + "\"" + " AND REFERENCED_COLUMN_NAME IS NOT NULL;";

			System.out.println("consulta fk:" + queryMetadata);
			rs = st.executeQuery(queryMetadata);
			// rs = st.executeQuery("SELECT * FROM posts;");
			while (rs.next()) {
				for (String parametro : parametros.getColumasSeleccionadas()) {
					String[] parametroDB = parametro.split(" ");
					String columna = parametroDB[1];
					if (columna.equals(rs.getString("COLUMN_NAME"))) {
						queryInnerjoin += "inner join " + rs.getString("REFERENCED_TABLE_NAME") + " on "
								+ rs.getString("TABLE_NAME").concat(".").concat(rs.getString("COLUMN_NAME")) + " = "
								+ rs.getString("REFERENCED_TABLE_NAME").concat(".")
										.concat(rs.getString("REFERENCED_COLUMN_NAME"));
					}
				}
			}
		}

		if (gestor.equals("postgresql")) {
			queryMetadata = "SELECT\r\n" + "    tc.table_schema, \r\n" + "    tc.constraint_name, \r\n"
					+ "    tc.table_name, \r\n" + "    kcu.column_name, \r\n"
					+ "    ccu.table_schema AS foreign_table_schema,\r\n"
					+ "    ccu.table_name AS foreign_table_name,\r\n"
					+ "    ccu.column_name AS foreign_column_name \r\n" + "FROM \r\n"
					+ "    information_schema.table_constraints AS tc \r\n"
					+ "    JOIN information_schema.key_column_usage AS kcu\r\n"
					+ "      ON tc.constraint_name = kcu.constraint_name\r\n"
					+ "      AND tc.table_schema = kcu.table_schema\r\n"
					+ "    JOIN information_schema.constraint_column_usage AS ccu\r\n"
					+ "      ON ccu.constraint_name = tc.constraint_name\r\n"
					+ "      AND ccu.table_schema = tc.table_schema\r\n"
					+ "WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name='" + tablaColumna + "';";

			rs = st.executeQuery(queryMetadata);

			while (rs.next()) {
				for (String parametro : parametros.getColumasSeleccionadas()) {
					String[] parametroDB = parametro.split(" ");
					String columna = parametroDB[1];
					if (columna.equals(rs.getString("column_name"))) {
						queryInnerjoin += "inner join " + rs.getString("foreign_table_name") + " on "
								+ rs.getString("table_name").concat(".") + "\"" + columna + "\"" + " = "
								+ rs.getString("foreign_table_name").concat(".") + "\""
								+ rs.getString("foreign_column_name") + "\"";
					}
				}
			}
		}

		if (gestor.equals("sqlserver")) {
			queryMetadata = "SELECT \r\n" + "   OBJECT_NAME(f.parent_object_id) TableName,\r\n"
					+ "   COL_NAME(fc.parent_object_id,fc.parent_column_id) ColName\r\n" + "FROM \r\n"
					+ "   sys.foreign_keys AS f\r\n" + "INNER JOIN \r\n" + "   sys.foreign_key_columns AS fc \r\n"
					+ "      ON f.OBJECT_ID = fc.constraint_object_id\r\n" + "INNER JOIN \r\n" + "   sys.tables t \r\n"
					+ "      ON t.OBJECT_ID = fc.referenced_object_id\r\n" + "WHERE \r\n"
					+ "   OBJECT_NAME (f.referenced_object_id) = '" + tablaColumna + "' ";

			System.out.println(queryMetadata);
			rs = st.executeQuery(queryMetadata);
			while (rs.next()) {

			}
		}

		Map<String, Object> respuesta = new HashMap<>();
		Conexion conexion = null;
		ConsultasDB consultasDB = new ConsultasDB();
		ConsultasDB consultaSubcategoria2 = new ConsultasDB();
		Producto productoExistente = null;
		try {
			queryTotal = queryGeneral + " " + queryInnerjoin + ";";
			System.out.println(queryTotal);
			rs = st.executeQuery(queryTotal);
			while (rs.next()) {
				productoExistente = productoService.findByNombreAndSucursal(
						rs.getString(parametros.getColumnas().getNombre()), userAuthenticate());
				if (productoExistente == null) {
					producto = new Producto();
					producto.setNombre(rs.getString(parametros.getColumnas().getNombre()));
					producto.setDescripcion(rs.getString(parametros.getColumnas().getDescripcion()));
					producto.setPrecio(Double.parseDouble(rs.getString(parametros.getColumnas().getPrecio())));
					producto.setCantidad(Integer.parseInt(rs.getString(parametros.getColumnas().getCantidad())));
					producto.setEstado(true);
					producto.setSucursal(userAuthenticate());
					producto.setSubcategoria(subCategoriaService.findByNombreAndSucursal(
							rs.getString(parametros.getColumnas().getSubcategoria_nombre()), userAuthenticate()));
					producto = productoService.save(producto);
				}

			}

			RelacionColumna relacionNueva = null;
			RelacionColumna relacion = new RelacionColumna();
			relacion.setNombre(parametros.getColumnas().getNombre());
			relacion.setDescripcion(parametros.getColumnas().getDescripcion());
			relacion.setPrecio(parametros.getColumnas().getPrecio());
			relacion.setCantidad(parametros.getColumnas().getCantidad());
			relacion.setSubcategoria(parametros.getColumnas().getSubcategoria());
			relacion.setSubcategoria_nombre(parametros.getColumnas().getSubcategoria_nombre());
			relacionNueva = relacionService.save(relacion);

			conexion = conexionService.getConexion(idConexion);

			consultaSubcategoria2.setConsulta(consultaSubcategoria);
			consultaSubcategoria2.setConexion(conexion);
			consultaSubcategoria2.setRelacion(relacion);
			consultaSubcategoria2.setTipoconsulta("subcategoria");
			consultaSubcategoria2 = consultaService.save(consultaSubcategoria2);

			consultasDB.setConsulta(queryTotal);
			consultasDB.setConexion(conexion);
			consultasDB.setRelacion(relacion);
			consultasDB.setTipoconsulta("producto");
			consultasDB = consultaService.save(consultasDB);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de los datos en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		DB.close();
		conexionBase().close();
		respuesta.put("mensaje", "Los datos han sido ingresado con exito!!");
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	// INICIA GESTION CONSULTAS
//	@Secured("ROLE_NEGOCIO")
//	@PostMapping("/consulta/{id}")
//	public ResponseEntity<?> consultaCrear(@RequestBody ConsultasDB consulta){
//		Map<String, Object> respuesta = new HashMap<>();
//		ConsultasDB consultaDBNueva = null;
//		
//		try {
//			consulta.setConexion(conexionService.getConexion(conexionGlobal.getConexion_id()));
//			consultaDBNueva = consultaService.save(consulta);
//		} catch (DataAccessException e) {
//			respuesta.put("mensaje", "Error al crear el registro en la DB!!");
//			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
//			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		respuesta.put("mensaje", "La consulta ha sido guardada con exito!!");
//		respuesta.put("consulta", consultaDBNueva);
//		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
//	}

	@Secured("ROLE_NEGOCIO")
	@PutMapping("/consulta/{id}")
	public ResponseEntity<?> consulta(@RequestBody ConsultasDB consulta, @PathVariable Long id) {

		Map<String, Object> respuesta = new HashMap<>();
		ConsultasDB consultaDBAcutal = null, consultaDBEditar = null;
		consultaDBAcutal = consultaService.findById(id);

		if (consultaDBAcutal == null) {
			respuesta.put("mensaje", "Error, no existe el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		try {
			consultaDBAcutal.setConsulta(consulta.getConsulta());
			consultaDBEditar = consultaService.save(consultaDBAcutal);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La consulta ha sido actualizado con exito!!");
		respuesta.put("consulta", consultaDBEditar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/consulta/{id}")
	public ResponseEntity<?> consulta(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		ConsultasDB consultaDBAcutal = null;

		try {
			consultaDBAcutal = consultaService.findById(id);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al obtener el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (consultaDBAcutal == null) {
			respuesta.put("mensaje", "Error,no existe el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ConsultasDB>(consultaDBAcutal, HttpStatus.OK);
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_consulta/{id}")
	public List<ConsultasDB> listConsultas(@PathVariable Long id) {
		List<ConsultasDB> consultas = new ArrayList<ConsultasDB>();
		// conexionGlobal2(conexionService.getConexion(id));
		System.out.println("ENTRANDO A CONSULTA:" + id);
		consultas = consultaService.findAllByConexion(conexionService.getConexion(id));
		// List<ConsultasDB> consultas =
		// consultaService.findAllByConexion(conexionService.getConexion(id));
		// System.out.println("CONSULTAS:" + consultas.get(0).getConsultas());
		return consultas;
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/consulta/ejecutar/{id}")
	public ResponseEntity<?> consultaEjecutar(@PathVariable Long id) throws SQLException, ClassNotFoundException {
		Map<String, Object> respuesta = new HashMap<>();
		ConsultasDB consultaDB = consultaService.findById(id);
		Connection DB = null;
		Producto producto = null, productoExistente = null;
		if (consultaDB == null) {
			respuesta.put("mensaje", "Error, no existe el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		try {
			conexionGlobal2(consultaDB.getConexion());
			DB = conexionBase();
			boolean valid = DB.isValid(50000);
			System.out.println(valid ? "Conexion establecida POST" : "TEST FAIL");
			Statement st = DB.createStatement();
			if (consultaDB.getTipoconsulta().equals("producto")) {
				ResultSet rs = st.executeQuery(consultaDB.getConsulta());
				while (rs.next()) {
					productoExistente = productoService.findByNombreAndSucursal(
							rs.getString(consultaDB.getRelacion().getNombre()), userAuthenticate());
					if (productoExistente == null) {
						producto = new Producto();
						producto.setNombre(rs.getString(consultaDB.getRelacion().getNombre()));
						producto.setDescripcion(rs.getString(consultaDB.getRelacion().getDescripcion()));
						producto.setPrecio(Double.parseDouble(rs.getString(consultaDB.getRelacion().getPrecio())));
						producto.setCantidad(Integer.parseInt(rs.getString(consultaDB.getRelacion().getCantidad())));
						producto.setEstado(true);
						producto.setSucursal(userAuthenticate());
						producto.setSubcategoria(subCategoriaService.findByNombreAndSucursal(
								rs.getString(consultaDB.getRelacion().getSubcategoria_nombre()), userAuthenticate()));
						producto = productoService.save(producto);
					}

				}
			} else if (consultaDB.getTipoconsulta().equals("subcategoria")) {
				ResultSet rs = st.executeQuery(consultaDB.getConsulta());
				SubCategoria subCategoria, subCategoriaExiste;
				while (rs.next()) {
					subCategoriaExiste = subCategoriaService.findByNombreAndSucursal(rs.getString(1),
							userAuthenticate());
					if (subCategoriaExiste == null) {
						subCategoria = new SubCategoria();
						subCategoria.setNombre(rs.getString(1));
						subCategoria.setSucursal(userAuthenticate());
						subCategoria.setEstado(true);
						subCategoria = subCategoriaService.save(subCategoria);
					}
				}
			}

			respuesta.put("mensaje", "Extracción de datos satisfactorio");
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al cargar los registros en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}

	// TERMINA CONSULTAS

	// FUNCIONES PARA CARGAR DATOS DESDE UN ARCHIVO .CSV

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/cargar_datos_excel/save")
	public ResponseEntity<?> cargarDatosCSV(@RequestBody ParametrosETLCSVModel parametrosCsv) {
		Map<String, Object> respuesta = new HashMap<>();
		SubCategoria subCategoria, subCategoriaNueva, nombreSubcategoriaDB;
		Producto producto, productoNuevo;
		String nombreSubcategoria, nombreProducto, descripcionProducto, precioProducto, cantidadProducto;
		double precioProductoD, cantidadProductoD;
		try {

			System.out.println(parametrosCsv.getColumasSeleccionadas());
			System.out.println(parametrosCsv.getColumnas().getNombre());
			System.out.println(parametrosCsv.getDatosExcel());
			System.out.println(parametrosCsv.getDatosExcel().size());
			if (parametrosCsv.getColumnas().getSubcategoria() > 0) {
				for (int j = 1; j < (parametrosCsv.getDatosExcel().size() - 1); j++) {
					for (int i = 0; i < parametrosCsv.getDatosExcel().get(j).size()
							- (parametrosCsv.getDatosExcel().get(j).size() - 1); i++) {
						System.out.println((j + 1) + " " + parametrosCsv.getDatosExcel().get(j)
								.get(parametrosCsv.getColumnas().getSubcategoria() - 1));
						nombreSubcategoria = parametrosCsv.getDatosExcel().get(j)
								.get(parametrosCsv.getColumnas().getSubcategoria() - 1);
						nombreSubcategoria = nombreSubcategoria.replaceAll("\\s", "");
						System.out.println("registro antes:" + nombreSubcategoria.length());
						if (nombreSubcategoria.isEmpty() || nombreSubcategoria.equals("")
								|| nombreSubcategoria.equals(" ")) {
							nombreSubcategoria = "S/S";
						}
						nombreSubcategoriaDB = subCategoriaService.findByNombreAndSucursal(nombreSubcategoria,
								userAuthenticate());
						System.out.println("SUBCATEGORIA PRIMER RESULTADO:" + nombreSubcategoriaDB);
						if (nombreSubcategoriaDB == null) {
							subCategoria = new SubCategoria();
							subCategoria.setNombre(nombreSubcategoria);
							subCategoria.setSucursal(userAuthenticate());
							subCategoriaNueva = subCategoriaService.save(subCategoria);
							System.out.println("registro despues:" + subCategoriaNueva.getNombre().length());
						}
					}
				}
				System.out.println("afuera segundo for");
				for (int i = 1; i < (parametrosCsv.getDatosExcel().size() - 1); i++) {
					for (int j = 0; j < parametrosCsv.getDatosExcel().get(i).size()
							- (parametrosCsv.getDatosExcel().get(j).size() - 1); j++) {
						System.out.println("adentro de segundo for");
						nombreProducto = parametrosCsv.getDatosExcel().get(i)
								.get(parametrosCsv.getColumnas().getNombre() - 1);
						descripcionProducto = parametrosCsv.getDatosExcel().get(i)
								.get(parametrosCsv.getColumnas().getDescripcion() - 1);
						cantidadProducto = parametrosCsv.getDatosExcel().get(i)
								.get(parametrosCsv.getColumnas().getCantidad() - 1);
						precioProducto = parametrosCsv.getDatosExcel().get(i)
								.get(parametrosCsv.getColumnas().getPrecio() - 1);

						System.out.println("nombre:" + nombreProducto);
						System.out.println("descripcion:" + descripcionProducto);
						System.out.println("cantidad:" + cantidadProducto);
						System.out.println("precio:" + precioProducto);

						nombreProducto = nombreProducto.replaceAll("\\s", "");
						descripcionProducto = descripcionProducto.replaceAll("\\s", "");
						cantidadProducto = cantidadProducto.replaceAll("\\s", "");
						precioProducto = precioProducto.replaceAll("\\s", "");

						if (nombreProducto.isEmpty() || nombreProducto.equals("") || nombreProducto.equals(" ")) {
							nombreProducto = "S/N";
						}
						if (descripcionProducto.isEmpty() || descripcionProducto.equals("")
								|| descripcionProducto.equals(" ")) {
							descripcionProducto = "S/D";
						}
						if (cantidadProducto.isEmpty() || cantidadProducto.equals("") || cantidadProducto.equals(" ")) {
							cantidadProducto = "0.00";
						}
						if (precioProducto.isEmpty() || precioProducto.equals("") || precioProducto.equals(" ")) {
							precioProducto = "0.00";
						}

						nombreSubcategoria = parametrosCsv.getDatosExcel().get(i)
								.get(parametrosCsv.getColumnas().getSubcategoria() - 1);
						nombreSubcategoria = nombreSubcategoria.replaceAll("\\s", "");
						nombreSubcategoriaDB = subCategoriaService.findByNombreAndSucursal(nombreSubcategoria,userAuthenticate());
						if (nombreSubcategoriaDB == null) {
							nombreSubcategoriaDB = null;
						}
						Producto productoExistente = productoService.findByNombreAndSucursal(nombreProducto,
								userAuthenticate());
						if (productoExistente == null) {
							producto = new Producto();
							producto.setNombre(nombreProducto);
							producto.setDescripcion(descripcionProducto);
							producto.setCantidad(Double.parseDouble(cantidadProducto));
							producto.setPrecio(Double.parseDouble(precioProducto));
							producto.setEstado(true);
							producto.setSubcategoria(nombreSubcategoriaDB);
							producto.setSucursal(userAuthenticate());
							productoNuevo = productoService.save(producto);
						}
					}
				}
			} else {
				respuesta.put("mensaje",
						"Error al realizar el ingreso de los datos de CSV en la DB, no existe subcategorias!!");
				return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_ACCEPTABLE);
			}

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de los datos de CSV en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "Los datos han sido ingresado con exito!!");
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);

	}
}
