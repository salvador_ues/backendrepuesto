package com.softwaremedida.springboot.backend.apirest.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="relacion_columna")
public class RelacionColumna extends AuditingModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	private int nombre;
	private int descripcion;
	private int precio;
	private int cantidad;
	private int subcategoria;
	private int subcategoria_nombre;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getNombre() {
		return nombre;
	}
	public void setNombre(int nombre) {
		this.nombre = nombre;
	}
	public int getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(int descripcion) {
		this.descripcion = descripcion;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(int subcategoria) {
		this.subcategoria = subcategoria;
	}
	public int getSubcategoria_nombre() {
		return subcategoria_nombre;
	}
	public void setSubcategoria_nombre(int subcategoria_nombre) {
		this.subcategoria_nombre = subcategoria_nombre;
	}
	
}
