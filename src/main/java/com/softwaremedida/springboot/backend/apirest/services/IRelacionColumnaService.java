package com.softwaremedida.springboot.backend.apirest.services;

import com.softwaremedida.springboot.backend.apirest.entity.RelacionColumna;

public interface IRelacionColumnaService {
	
	public abstract RelacionColumna findById(Long id);
	public abstract RelacionColumna save(RelacionColumna relacion);

}
