package com.softwaremedida.springboot.backend.apirest.services;

import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

public interface IUsuarioService {
	public Usuario finByUsername(String username);
	public Usuario save(Usuario usuario);
	public Usuario findById(Long id);
}
