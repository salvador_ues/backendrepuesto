package com.softwaremedida.springboot.backend.apirest.models;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

public class UsuarioAdmin {
	
	private Usuario usuario;
	private Administradores administrador;
	//private Sucursal sucursal;
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Administradores getAdministrador() {
		return administrador;
	}
	public void setAdministrador(Administradores administrador) {
		this.administrador = administrador;
	}
//	public Sucursal getSucursal() {
//		return sucursal;
//	}
//	public void setSucursal(Sucursal sucursal) {
//		this.sucursal = sucursal;
//	}
	
	
	
}
