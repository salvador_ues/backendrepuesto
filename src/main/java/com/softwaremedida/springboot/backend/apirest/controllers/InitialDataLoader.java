package com.softwaremedida.springboot.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.softwaremedida.springboot.backend.apirest.entity.AdministradorTotal;
import com.softwaremedida.springboot.backend.apirest.entity.Rol;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.repository.AdminTotalRepository;
import com.softwaremedida.springboot.backend.apirest.services.IRolService;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	boolean alreadySetup = false;
	
	@Autowired
	private AdminTotalRepository adminTotalRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private IRolService rolService;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if(adminTotalRepository.findByCodigo("adminTotal")==null) {
			AdministradorTotal admin = new AdministradorTotal();
			admin.setCodigo("adminTotal");
			adminTotalRepository.save(admin);
		}
		Rol rol=null;
		if(rolService.findByNombre("ROLE_ADMIN")==null) {
			rol = new Rol();
			rol.setNombre("ROLE_ADMIN");
			rolService.save(rol);
		}
		if(rolService.findByNombre("ROLE_NEGOCIO")==null) {
			rol = new Rol();
			rol.setNombre("ROLE_NEGOCIO");
			rolService.save(rol);
		}
		if(rolService.findByNombre("ROLE_USER")==null) {
			rol = new Rol();
			rol.setNombre("ROLE_USER");
			rolService.save(rol);
		}
		
		if(usuarioService.finByUsername("admin")==null) {
			Usuario usuario = new Usuario();
			usuario.setUsername("admin");
			usuario.setPassword(passwordEncoder.encode("12345"));
			usuario.setEmail("admin@gmail.com");
			usuario.setEnabled(true);
			List<Rol> roles = new ArrayList<Rol>();
			roles.add(rolService.findByNombre("ROLE_ADMIN"));
			usuario.setRoles(roles);
			usuario = usuarioService.save(usuario);
		}
		
		alreadySetup = true;
	}
	
	
}
