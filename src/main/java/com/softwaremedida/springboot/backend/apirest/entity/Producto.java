package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="productos")
public class Producto extends AuditingModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long producto_id;
	
	@NotEmpty(message = "no puede estar vacio")
	private String nombre;
	
	@NotEmpty(message = "no puede estar vacio")
	private String descripcion;
	
	@NotNull(message = "no puede estar vacio")
	@Column(precision = 6, scale=2,nullable = false)
	private double precio;
	
	@NotNull(message = "no puede estar vacio")
	@Column(precision = 6, scale=2,nullable = false)
	private double cantidad;
		
	@NotNull(message="la subcategoria no puede ser vacia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="subcategoria_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private SubCategoria subcategoria;
	
	private boolean estado;
	
	@NotNull(message="la sucursal no puede ser vacia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sucursal_id",nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Sucursal sucursal;
	
//	@OneToMany(mappedBy = "producto")
//	private Set<DetalleFactura> detalleFacturas;

	public Long getProducto_id() {
		return producto_id;
	}

	public void setProducto_id(Long producto_id) {
		this.producto_id = producto_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public SubCategoria getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(SubCategoria subcategoria) {
		this.subcategoria = subcategoria;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	public Producto() {
		super();
	}

	public Producto(@NotEmpty(message = "no puede estar vacio") String nombre,
			@NotEmpty(message = "no puede estar vacio") String descripcion,
			@NotNull(message = "no puede estar vacio") double precio,
			@NotNull(message = "no puede estar vacio") double cantidad,
			@NotNull(message = "la subcategoria no puede ser vacia") SubCategoria subcategoria, boolean estado,
			@NotNull(message = "la sucursal no puede ser vacia") Sucursal sucursal) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.cantidad = cantidad;
		this.subcategoria = subcategoria;
		this.estado = estado;
		this.sucursal = sucursal;
	}
	
//	public Set<DetalleFactura> getDetalleFacturas() {
//		return detalleFacturas;
//	}
//
//	public void setDetalleFacturas(Set<DetalleFactura> detalleFacturas) {
//		this.detalleFacturas = detalleFacturas;
//	}

	
	

	
	
	
	

}
