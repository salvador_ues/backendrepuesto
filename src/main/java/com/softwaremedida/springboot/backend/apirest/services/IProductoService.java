package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

public interface IProductoService {

	public abstract List<Producto> findAllBySucursal(Sucursal sucursal);
	public abstract Page<Producto> findAllBySucursal(Pageable pageable, Sucursal sucursal); 
	public abstract List<Producto> findAll();
	public abstract Page<Producto> findAll(Pageable pageable);
	public abstract List<Producto> findAllBySubCategoria(SubCategoria subCategoria);
	public abstract Page<Producto> findAllBySubCategoria(SubCategoria subCategoria,Pageable page);
	public abstract Producto save(Producto producto);
	public abstract Producto findById(Long id);
	public abstract Producto findByNombreAndSucursal(String nombre, Sucursal sucursal);
	
}
