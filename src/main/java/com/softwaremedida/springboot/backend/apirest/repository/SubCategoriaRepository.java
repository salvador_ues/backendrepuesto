package com.softwaremedida.springboot.backend.apirest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

@Repository
public interface SubCategoriaRepository extends JpaRepository<SubCategoria, Long>{

	public SubCategoria findByNombre(String nombre);
	public List<SubCategoria> findBySucursal(Sucursal sucursal);
	public Page<SubCategoria> findBySucursal(Sucursal sucursal,Pageable pageable);
	
	@Query("SELECT s FROM SubCategoria s WHERE s.nombre = ?1 and s.sucursal = ?2 ")
	public SubCategoria findByNombreAndSucursal(String nombre,Sucursal sucursal);

	@Query("from SubCategoria s where s.estado = ?1")
	public List<SubCategoria> findByEstado(boolean estado);
}
