package com.softwaremedida.springboot.backend.apirest.models;

import com.softwaremedida.springboot.backend.apirest.entity.Producto;

public class CartLineModel {
	private Producto producto;
	private Integer cantidad;
	private Double lineaTotal = 0D;

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getLineaTotal() {
		return lineaTotal;
	}

	public void setLineaTotal(Double lineaTotal) {
		this.lineaTotal = lineaTotal;
	}
}
