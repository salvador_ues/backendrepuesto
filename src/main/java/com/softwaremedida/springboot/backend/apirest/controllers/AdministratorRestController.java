package com.softwaremedida.springboot.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Comision;
import com.softwaremedida.springboot.backend.apirest.entity.Orden;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.Rol;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.models.UsuarioAdmin;
import com.softwaremedida.springboot.backend.apirest.repository.AdminTotalRepository;
import com.softwaremedida.springboot.backend.apirest.services.ComisionServiceImp;
import com.softwaremedida.springboot.backend.apirest.services.IAdministradorService;
import com.softwaremedida.springboot.backend.apirest.services.IComisionService;
import com.softwaremedida.springboot.backend.apirest.services.IOrdenService;
import com.softwaremedida.springboot.backend.apirest.services.IRolService;
import com.softwaremedida.springboot.backend.apirest.services.ISubCategoriaService;
import com.softwaremedida.springboot.backend.apirest.services.ISucursalService;
import com.softwaremedida.springboot.backend.apirest.services.ProductoServiceImp;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api_admin")
public class AdministratorRestController {

	@Autowired
	private IComisionService comisionService;

	@Autowired
	private ISucursalService sucursalService;

	@Autowired
	private IAdministradorService administradorSucursalService;

	@Autowired
	private AdminTotalRepository adminTotalRepository;
	
	@Autowired
	private IOrdenService ordenService;
	
	@Autowired
	private IRolService rolService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ProductoServiceImp productoService;
	
	@Autowired
	private ISubCategoriaService subCategoriaService;
	
	private Authentication userAuthenticate() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	// Gestion de comision
	@Secured("ROLE_ADMIN")
	@GetMapping("/comisiones")
	public List<Comision> comisiones() {

		return comisionService.findAll();
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/comisiones/page/{page}")
	public Page<Comision> comisiones(@PathVariable Integer page) {
		return comisionService.findAll(PageRequest.of(page, 4));
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/comision/{id}")
	public ResponseEntity<?> mostrarComision(@PathVariable Long id) {
		Comision comision = null;
		Map<String, Object> respuesta = new HashMap<>();

		try {
			comision = comisionService.findById(id);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar la busqueda del registro con ID:" + id);
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (comision == null) {
			respuesta.put("mensaje", "La comision con ID:" + id + " no existe en la DB!");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Comision>(comision, HttpStatus.OK);
	}

	// se usa @RequestBody ya que el input viene en formato JSON y viene en la
	// request y lo mapee al objeto comision
	@Secured("ROLE_ADMIN")
	@PostMapping("/comision") // @ResponseStatus(HttpStatus.CREATED) //se envia codigo 201 que se ha ejecutado
								// correctamente el ingreso
	public ResponseEntity<?> crearComision(@RequestBody Comision comision) {
		Comision comisionNueva = null, comisionActual = null;
		Map<String, Object> respuesta = new HashMap<>();
		try {
			if (comision.isEstado()==true) {

				comisionActual = comisionService.findComisionByEstado(true);
				if (comisionActual != null) {
					comisionActual.setEstado(false);
					comisionActual = comisionService.add(comisionActual);
				}
			}
			comision.setAdministradorTotal(adminTotalRepository.findById(1L).orElse(null));
			comisionNueva = comisionService.add(comision);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de la comisión en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "El producto ha sido creado con exito!!");
		respuesta.put("producto", comisionNueva);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@PutMapping("/comision/{id}")
	public ResponseEntity<?> modificarComision(@RequestBody Comision comision, @PathVariable Long id) {
		Comision comisionActual = comisionService.findById(id);
		Comision comisionModificar = null, comisionAnterior = null;
		Map<String, Object> respuesta = new HashMap<>();

		if (comisionActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			// comisionActual.setAdministradorTotal(adminTotalRepository.findById(1L).orElse(null));
			if (comision.isEstado()) {
				comisionAnterior = comisionService.findComisionByEstado(true);
				if (comisionAnterior != null) {
					comisionAnterior.setEstado(false);
					comisionAnterior = comisionService.add(comisionAnterior);
				}
			}
			comisionActual.setComision(comision.getComision());
			comisionActual.setEstado(comision.isEstado());
			comisionModificar = comisionService.add(comisionActual);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "El producto ha sido actualizado con exito!!");
		respuesta.put("producto", comisionModificar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/comision/{id}")
	public ResponseEntity<?> eliminarComision(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Comision comisionActual = comisionService.findById(id);
		Comision comisionEliminar = null;
		if (comisionActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			comisionActual.setEstado(false);
			comisionEliminar = comisionService.add(comisionActual);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al bloquear el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La comision ha sido bloqueada con exito!!");
		respuesta.put("comision", comisionEliminar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}

	// Termina gestion de comision

	// Inicio de gestion de empresas
	@Secured("ROLE_ADMIN")
	@GetMapping("/sucursales")
	public List<Sucursal> sucursales() {
		Administradores admin = new Administradores("prueba2", true);
		administradorSucursalService.save(admin);
		return sucursalService.findAll();
	}

	@GetMapping("/sucursales/page/{page}")
	public Page<Sucursal> sucursales(@PathVariable Integer page) {
		return sucursalService.findAll(PageRequest.of(page, 4));
	}

	
	@Secured("ROLE_ADMIN")
	@GetMapping("/sucursal/{id}")
	public ResponseEntity<?> mostrarSucursal(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Sucursal sucursal = null;
		System.out.println("SUCURSAL ID:" + id);
		try {
			sucursal = sucursalService.findById(id);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar la busqueda del registro con ID:" + id);
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (sucursal == null) {
			respuesta.put("mensaje", "La sucursal con ID:" + id + " no existe en la DB!");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Sucursal>(sucursal, HttpStatus.OK);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping("/sucursal")
	public ResponseEntity<?> crearSucursal(@Valid @RequestBody Sucursal sucursal, BindingResult result) {

		System.out.println("VALORES DESDE WEB:" + sucursal.getNombre());
		System.out.println("VALORES DESDE WEB:" + sucursal.getEmail());
		// System.out.println("VALORES DESDE WEB:"+sucursal.isEstado());
		System.out.println("VALORES DESDE WEB:" + sucursal.getAdministrador().getNombreNegocio());
		Sucursal sucursalNueva = null;
		Map<String, Object> respuesta = new HashMap<>();

		if (result.hasErrors()) {

			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());

			System.out.println("ERRORES:" + errors);

		}
		Sucursal sucursal2 = new Sucursal();
		try {
			// sucursal.setEstado(true);
			// sucursal.setAdministrador(administradorSucursalService.findById(1L));
			// sucursalNueva = sucursalService.saveSucursal(new
			// Sucursal(sucursal.getAdministrador(),sucursal.getEmail(),sucursal.getNombre()));
			sucursal.setEstado(true);
			sucursalNueva = sucursalService.saveSucursal(sucursal);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de la sucursal en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La sucursal ha sido creado con exito!!");
		respuesta.put("producto", sucursalNueva);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@PutMapping("/sucursal/{id}")
	public ResponseEntity<?> modificarSucursal(@RequestBody Sucursal sucursal, @PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Sucursal sucursalModificar = null;
		Sucursal sucursalActual = sucursalService.findById(id);

		if (sucursalActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			sucursalActual.setNombre(sucursal.getNombre());
			sucursalActual.setEmail(sucursal.getEmail());
			sucursalActual.setAdministrador(sucursal.getAdministrador());
			sucursalModificar = sucursalService.saveSucursal(sucursalActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El producto ha sido actualizado con exito!!");
		respuesta.put("producto", sucursalModificar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/sucursal/{id}")
	public ResponseEntity<?> eliminarSucursal(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Sucursal sucursalEliminar = null;
		Sucursal sucursalActual = sucursalService.findById(id);
		try {
			sucursalActual.setEstado(false);
			sucursalEliminar = sucursalService.saveSucursal(sucursalActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al bloquear el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La sucursal ha sido eliminado con exito!!");
		respuesta.put("producto", sucursalEliminar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}
	// Termina gestion de empresas

	// Gestion de administradores
	@Secured("ROLE_ADMIN")
	@GetMapping("/administradores_sucursales")
	public List<Administradores> administradores() {
		return administradorSucursalService.findAll();
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/administradores_sucursales/page/{page}")
	public Page<Administradores> administradores(@PathVariable Integer page) {
		return administradorSucursalService.findAll(PageRequest.of(page, 4));
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/administrador/{id}")
	public ResponseEntity<?> getAdministrador(@PathVariable Long id) {
		Administradores admin = null;
		Usuario usuario = null;
		UsuarioAdmin usuarioAdmin = new UsuarioAdmin();
		Map<String, Object> respuesta = new HashMap<>();
		Authentication auth = userAuthenticate();
		System.out.println("usuario logueado username:"+auth.getName());
		try {
			System.out.println("ID de admin:"+id);
			admin = administradorSucursalService.findById(id);
			System.out.println("admin:"+admin.getNombreNegocio());
			
			usuario = admin.getUsuario();
			System.out.println("usuario:"+usuario.getNombre());
			
			usuarioAdmin.setAdministrador(admin);
			usuarioAdmin.setUsuario(usuario);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el pedido del admin en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (admin == null) {
			respuesta.put("mensaje", "El admin con ID:" + id + " no existe en la DB!");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UsuarioAdmin>(usuarioAdmin, HttpStatus.OK);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping("/administrador")
	public ResponseEntity<?> crearAdministrador(@RequestBody UsuarioAdmin usuarioAdmin) {
		Administradores adminNuevo = null,admin;
		Usuario usuarioNuevo=null,usuario;
		Sucursal sucursal=null,sucursalNueva=null;
		Map<String, Object> respuesta = new HashMap<>();

		try { 
			List<Rol> roles = new ArrayList<Rol>();
			roles.add(rolService.findByNombre("ROLE_NEGOCIO"));
			
			usuario = usuarioAdmin.getUsuario();
			usuario.setEnabled(true);
			usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
			usuario.setRoles(roles);
			usuarioNuevo = usuarioService.save(usuario);
			
			System.out.println("Admin:"+usuarioAdmin.getAdministrador().getNombreNegocio());
			//String nombreNegocio = usuarioAdmin.getAdministrador().getNombreNegocio();
			
			admin = usuarioAdmin.getAdministrador();
			admin.setEstado(true);
			admin.setUsuario(usuarioNuevo);
			adminNuevo = administradorSucursalService.save(admin);
			
			sucursal = new Sucursal();
			sucursal.setNombre(usuarioAdmin.getUsuario().getNombre());
			sucursal.setEstado(true);
			sucursal.setAdministrador(adminNuevo);
			sucursalNueva = sucursalService.saveSucursal(sucursal);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso del admin en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El admin ha sido creado con exito!!");
		respuesta.put("admin", adminNuevo);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@PutMapping("/administrador/{id}")
	public ResponseEntity<?> modificarAdministrador(@RequestBody UsuarioAdmin usuarioAdmin, @PathVariable Long id) {
		Administradores adminActual = null, adminModificar = null;
		Usuario usuarioActual=null,usuarioModificar=null;
		Map<String, Object> respuesta = new HashMap<>();
		adminActual = administradorSucursalService.findById(id);
		Sucursal sucursalActual = null,sucursalModificar=null;
		if (adminActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			usuarioActual = adminActual.getUsuario();
			usuarioActual.setUsername(usuarioAdmin.getUsuario().getUsername());
			usuarioActual.setEmail(usuarioAdmin.getUsuario().getEmail());
			usuarioActual.setNombre(usuarioAdmin.getUsuario().getNombre());
			usuarioModificar = usuarioService.save(usuarioActual);
			
			
			adminActual.setNombreNegocio(usuarioAdmin.getAdministrador().getNombreNegocio());
			adminModificar = administradorSucursalService.save(adminActual);
			
			sucursalActual = sucursalService.findByAdministrador(adminActual);
			sucursalActual.setNombre(usuarioAdmin.getUsuario().getNombre());
			sucursalModificar = sucursalService.saveSucursal(sucursalActual);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El admin ha sido modificado con exito!!");
		respuesta.put("admin", adminModificar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/administrador/{id}")
	public ResponseEntity<?> eliminarAdministrador(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Administradores adminActual = administradorSucursalService.findById(id);
		Administradores adminBloquear;
		if (adminActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			Usuario usuario = usuarioService.findById(adminActual.getUsuario().getId());
			usuario.setEnabled(false);
			usuario = usuarioService.save(usuario);
			
			adminActual.setEstado(false);
			adminBloquear = administradorSucursalService.save(adminActual);
			
			Sucursal sucursal = sucursalService.findByAdministrador(adminActual);
			sucursal.setEstado(false);
			sucursal = sucursalService.saveSucursal(sucursal);
			
			Producto productoActualizado = null;
			List<Producto> productos = productoService.findAllBySucursal(sucursal);
			for(Producto producto:productos) {
				producto.setEstado(false);
				productoActualizado = productoService.save(producto);
			}
			
			SubCategoria subcategoriaActualizar = null;
			List<SubCategoria> subcategorias = subCategoriaService.findBySucursal(sucursal);
			for(SubCategoria subcategoria:subcategorias) {
				subcategoria.setEstado(false);
				subcategoriaActualizar = subCategoriaService.save(subcategoria);
			}
			
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al bloquear el registro en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El admin ha sido bloqueado con exito!!");
		respuesta.put("admin", adminBloquear);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	// Termina gestion de administradores
	
	//Gestion ordenes 
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/list_ordenes/page/{page}")
	public Page<Orden> listOrdentes(@PathVariable Integer page){
		
		return ordenService.findAll(PageRequest.of(page, 8));
	}
	
	//USAR SI ES NECESARIO
//	@GetMapping("/orden/{id}")
//	public ResponseEntity<?> orden(@PathVariable Long id){
//		Map<String, Object> respuesta = new HashMap<>();
//		Orden orden = null;
//		try {
//			orden = ordenService.findOrdenById(id);
//			
//		} catch (DataAccessException e) {
//			respuesta.put("mensaje", "Error al realizar el pedido de la orden en la DB!!");
//			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
//			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		
//		if (orden == null) {
//			respuesta.put("mensaje", "La orden con ID:" + id + " no existe en la DB!");
//			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
//		}
//		return new ResponseEntity<Orden>(orden, HttpStatus.OK);
//		
//	}
	
	//Termina ordenes
}
