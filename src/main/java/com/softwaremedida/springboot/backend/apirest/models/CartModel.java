package com.softwaremedida.springboot.backend.apirest.models;

public class CartModel {
		
	private Integer cantidadItem =0;
	
	private double precioCart=0D;
	
	private java.util.List<CartLineModel> lineas;

	public Integer getCantidadItem() {
		return cantidadItem;
	}

	public void setCantidadItem(Integer cantidadItem) {
		this.cantidadItem = cantidadItem;
	}

	public double getPrecioCart() {
		return precioCart;
	}

	public void setPrecioCart(double precioCart) {
		this.precioCart = precioCart;
	}

	public java.util.List<CartLineModel> getLineas() {
		return lineas;
	}

	public void setLineas(java.util.List<CartLineModel> lineas) {
		this.lineas = lineas;
	}

	

}
