package com.softwaremedida.springboot.backend.apirest.models;

import java.util.List;

public class TablaColumnaModel {
	
	private String tabla;
	private List<String> columnas;
	public String getTabla() {
		return tabla;
	}
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}
	
	public List<String> getColumnas() {
		return columnas;
	}
	public void setColumnas(List<String> columnas) {
		this.columnas = columnas;
	}
	
	public TablaColumnaModel() {}
	public TablaColumnaModel(String tabla, List<String> columnas) {
		super();
		this.tabla = tabla;
		this.columnas = columnas;
	}
	
	
	
	
}
