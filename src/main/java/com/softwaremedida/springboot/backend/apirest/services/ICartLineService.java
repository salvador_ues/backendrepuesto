package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import com.softwaremedida.springboot.backend.apirest.entity.CartLine;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;

public interface ICartLineService {
	
	public abstract CartLine save(CartLine cartLine);
	public abstract List<CartLine> findByProducto(Producto producto);

}
