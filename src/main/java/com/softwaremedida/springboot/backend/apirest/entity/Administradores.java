package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.userdetails.User;

@Entity
@Table(name="administrador")
public class Administradores extends AuditingModel{
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name="id_administrador")
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "administrador_generator")
//	@SequenceGenerator(name="administrador_generator", sequenceName = "administrador_seq")
//	@Column(name = "id_administrador", updatable = false, nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long administrador_id;
	
	@NotEmpty(message = "no pude estar vacio")
	@Column(unique = true)
	private String nombreNegocio;
	
	@OneToOne
	@JoinColumn(name="usuario_id",referencedColumnName = "id")
	private Usuario usuario;
	

	public boolean estado;
	
	public Long getAdministrador_id() {
		return administrador_id;
	}

	public void setAdministrador_id(Long administrador_id) {
		this.administrador_id = administrador_id;
	}

	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}
	
	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Administradores(@NotEmpty String nombreNegocio,boolean estado) {
		super();
		this.nombreNegocio = nombreNegocio;
		this.estado= estado;
	}
	
	
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Administradores(){}	
	

}
