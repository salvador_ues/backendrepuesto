package com.softwaremedida.springboot.backend.apirest.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.services.IAdministradorService;
import com.softwaremedida.springboot.backend.apirest.services.IUsuarioService;

@Component
public class InfoAditionaToken implements TokenEnhancer {
	
	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private IAdministradorService adminService;
	
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> info = new HashMap<>();
		Usuario usuario = usuarioService.finByUsername(authentication.getName());
	//	Administrador admin = adminService.findByUsuario(usuario);
		
		info.put("nombre_usuario",usuario.getUsername());
		info.put("email", usuario.getEmail());
		//info.put("adminstrador", admin.getAdministrador_id());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		return accessToken;
	}
	

}
