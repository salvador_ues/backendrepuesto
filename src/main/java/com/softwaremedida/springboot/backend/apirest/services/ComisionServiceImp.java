package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.softwaremedida.springboot.backend.apirest.entity.Comision;
import com.softwaremedida.springboot.backend.apirest.repository.ComisionRepository;

@Service
public class ComisionServiceImp implements IComisionService{
	
	@Autowired
	private ComisionRepository comisionRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Comision> findAll() {
		return (List<Comision>) comisionRepository.findAll();
	}

	@Override
	@Transactional
	public Comision add(Comision comision) {
		
		return comisionRepository.save(comision);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		comisionRepository.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Comision findById(Long id) {
		
		return comisionRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Comision> findAll(Pageable pageable) {
		
		return comisionRepository.findAll(pageable);
	}

	@Override
	public Comision findComisionByEstado(boolean estado) {
		
		return comisionRepository.findComisionByEstado(estado);
	}

}
