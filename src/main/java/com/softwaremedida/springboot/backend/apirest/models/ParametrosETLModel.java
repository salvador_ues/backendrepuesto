package com.softwaremedida.springboot.backend.apirest.models;

import java.util.List;

public class ParametrosETLModel {
	
	private ColumnasDBCentralModel columnas;
	//private List<ColumnasSeleccionadas> columasSeleccionadas;
	private List<String> columasSeleccionadas;
	
	public ColumnasDBCentralModel getColumnas() {
		return columnas;
	}
	public void setColumnas(ColumnasDBCentralModel columnas) {
		this.columnas = columnas;
	}
//	public List<ColumnasSeleccionadas> getColumasSeleccionadas() {
//		return columasSeleccionadas;
//	}
//	public void setColumasSeleccionadas(List<ColumnasSeleccionadas> columasSeleccionadas) {
//		this.columasSeleccionadas = columasSeleccionadas;
//	}
	public List<String> getColumasSeleccionadas() {
		return columasSeleccionadas;
	}
	public void setColumasSeleccionadas(List<String> columasSeleccionadas) {
		this.columasSeleccionadas = columasSeleccionadas;
	}
	
	
	

}
