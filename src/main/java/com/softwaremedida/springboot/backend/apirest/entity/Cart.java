package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="carro")
public class Cart extends AuditingModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long carro_id;
	
	private Integer cantidadItem =0;
	
	private Double precioCart=0D;
	
	private Double comision=0D;
	
	private Double montoComision=0D;
	
	//private java.util.List<CartLine> lineas;

	public Integer getCantidadItem() {
		return cantidadItem;
	}

	public void setCantidadItem(Integer cantidadItem) {
		this.cantidadItem = cantidadItem;
	}

	public Double getPrecioCart() {
		return precioCart;
	}

	public void setPrecioCart(Double precioCart) {
		this.precioCart = precioCart;
	}

	public Long getCarro_id() {
		return carro_id;
	}

	public void setCarro_id(Long carro_id) {
		this.carro_id = carro_id;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public Double getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(Double montoComision) {
		this.montoComision = montoComision;
	}
	
	

//	public java.util.List<CartLine> getLineas() {
//		return lineas;
//	}
//
//	public void setLineas(java.util.List<CartLine> lineas) {
//		this.lineas = lineas;
//	}

}


