package com.softwaremedida.springboot.backend.apirest.models;

public class ColumnasDBCentralModel {
	
	private int nombre;
	private int descripcion;
	private int precio;
	private int cantidad;
	private int subcategoria;
	private int subcategoria_nombre;
	public int getNombre() {
		return nombre;
	}
	public void setNombre(int nombre) {
		this.nombre = nombre;
	}
	public int getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(int descripcion) {
		this.descripcion = descripcion;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(int subcategoria) {
		this.subcategoria = subcategoria;
	}
	public int getSubcategoria_nombre() {
		return subcategoria_nombre;
	}
	public void setSubcategoria_nombre(int subcategoria_nombre) {
		this.subcategoria_nombre = subcategoria_nombre;
	}
	
	

}
