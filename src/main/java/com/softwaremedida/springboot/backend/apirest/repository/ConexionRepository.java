package com.softwaremedida.springboot.backend.apirest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

@Repository("conexionRepository")
public interface ConexionRepository extends JpaRepository<Conexion, Long>{
	
	@Query("select c from Conexion c where c.estado = true and c.sucursal = :sucursal")
	public List<Conexion> findAllByEstado(@Param("sucursal") Sucursal sucursal);
	
	@Query("select c from Conexion c where c.estado = true and c.sucursal = :sucursal")
	public Page<Conexion> findAllByEstado(@Param("sucursal") Sucursal sucursal,Pageable pageable);
	
}
