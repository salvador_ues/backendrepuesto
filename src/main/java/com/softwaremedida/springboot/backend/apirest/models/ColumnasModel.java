package com.softwaremedida.springboot.backend.apirest.models;

import java.util.List;

public class ColumnasModel {
	
	private List<String> columnas;


	public List<String> getColumnas() {
		return columnas;
	}

	public void setColumnas(List<String> columnas) {
		this.columnas = columnas;
	}

	public ColumnasModel(List<String> columnas) {
		super();
		this.columnas = columnas;
	}
	
	public ColumnasModel() {}
}
