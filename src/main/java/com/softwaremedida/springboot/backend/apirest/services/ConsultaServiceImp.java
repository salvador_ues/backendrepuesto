package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.ConsultasDB;
import com.softwaremedida.springboot.backend.apirest.repository.ConsultasRepository;

@Service("consultaServiceImp")
public class ConsultaServiceImp implements IConsultaService {
	
	@Autowired
	private ConsultasRepository consultaRepository;
	
	@Override
	public ConsultasDB save(ConsultasDB consulta) {
		
		return consultaRepository.save(consulta);
	}

	@Override
	public ConsultasDB findById(Long id) {
		
		return consultaRepository.findById(id).orElse(null);
	}

	@Override
	public Page<ConsultasDB> findAllByConexion(Conexion conexion, Pageable pageable) {
		
		return consultaRepository.findByConexion(conexion, pageable);
	}

	@Override
	public List<ConsultasDB> findAllByConexion(Conexion conexion) {
		
		return consultaRepository.findAllByConexion(conexion);
	}
	
	
}
