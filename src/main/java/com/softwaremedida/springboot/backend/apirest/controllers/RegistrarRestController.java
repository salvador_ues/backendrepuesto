package com.softwaremedida.springboot.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softwaremedida.springboot.backend.apirest.entity.Rol;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.services.IRolService;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api_registrar")
public class RegistrarRestController {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private IRolService rolService;
	
	@PostMapping("/registrar")
	public ResponseEntity<?> registrarUsuario(@RequestBody Usuario usuario){
		Map<String, Object> respuesta = new HashMap<>();
		Usuario usuarioNuevo = null;
		try {
			List<Rol> roles = new ArrayList<Rol>();
			roles.add(rolService.findByNombre("ROLE_USER"));
			usuario.setRoles(roles);
			usuario.setEnabled(true);
			usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
			usuarioNuevo = usuarioService.save(usuario);
			
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el registro");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		respuesta.put("mensaje", "La cuenta ha sido creado con exito!!");
		respuesta.put("usuario", usuarioNuevo);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
		
	}
	

}
