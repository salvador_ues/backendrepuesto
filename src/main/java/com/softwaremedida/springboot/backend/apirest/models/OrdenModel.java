package com.softwaremedida.springboot.backend.apirest.models;

import com.softwaremedida.springboot.backend.apirest.entity.Cart;

public class OrdenModel {

	private Long orden_id;
	
	
	private String nombre_remitente;
	
	
	private String direccion;
	

	public String ciudad;
	
	private boolean enviado=false;
	
	private CartModel cart;

	public Long getOrden_id() {
		return orden_id;
	}

	public void setOrden_id(Long orden_id) {
		this.orden_id = orden_id;
	}

	public String getNombre_remitente() {
		return nombre_remitente;
	}

	public void setNombre_remitente(String nombre_remitente) {
		this.nombre_remitente = nombre_remitente;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public boolean isEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public CartModel getCart() {
		return cart;
	}

	public void setCart(CartModel cart) {
		this.cart = cart;
	}

	
	

}
