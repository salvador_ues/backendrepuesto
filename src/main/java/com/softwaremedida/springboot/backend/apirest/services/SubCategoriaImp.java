package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.repository.SubCategoriaRepository;

@Service
public class SubCategoriaImp implements ISubCategoriaService{
	
	@Autowired
	private SubCategoriaRepository subCategoriaRepository;
	
	@Override
	public SubCategoria findById(Long id) {
		
		return subCategoriaRepository.findById(id).orElse(null);
	}

	@Override
	public SubCategoria save(SubCategoria nombre) {
	
		return subCategoriaRepository.save(nombre);
	}

	@Override
	public SubCategoria findByNombre(String nombre) {
		
		return subCategoriaRepository.findByNombre(nombre);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<SubCategoria> findBySucursal(Sucursal sucursal,Pageable pageable) {
	
		return subCategoriaRepository.findBySucursal(sucursal, pageable);
	}

	@Override
	public List<SubCategoria> findBySucursal(Sucursal sucursal) {
		
		return subCategoriaRepository.findBySucursal(sucursal);
	}

	@Override
	public List<SubCategoria> findAll() {
		
		return subCategoriaRepository.findByEstado(true);
	}

	@Override
	public SubCategoria findByNombreAndSucursal(String nombre, Sucursal sucursal) {
		
		return subCategoriaRepository.findByNombreAndSucursal(nombre, sucursal);
	}

}
