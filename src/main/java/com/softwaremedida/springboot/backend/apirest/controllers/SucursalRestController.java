package com.softwaremedida.springboot.backend.apirest.controllers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.softwaremedida.springboot.backend.apirest.entity.CartLine;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.models.ParametrosETLCSVModel;
import com.softwaremedida.springboot.backend.apirest.services.IAdministradorService;
import com.softwaremedida.springboot.backend.apirest.services.ICartLineService;
import com.softwaremedida.springboot.backend.apirest.services.ICartService;
import com.softwaremedida.springboot.backend.apirest.services.IOrdenService;
import com.softwaremedida.springboot.backend.apirest.services.IProductoService;
import com.softwaremedida.springboot.backend.apirest.services.ISubCategoriaService;
import com.softwaremedida.springboot.backend.apirest.services.ISucursalService;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api_sucursal")
public class SucursalRestController {

	@Autowired
	private ISucursalService sucursalService;

	@Autowired
	private IProductoService productoService;

	@Autowired
	private ISubCategoriaService subCategoriaService;

	@Autowired
	private ICartService cartService;

	@Autowired
	private ICartLineService cartLineService;

	@Autowired
	private IOrdenService ordenService;

	@Autowired
	private IAdministradorService administradorService;

	@Autowired
	private UsuarioService usuarioService;

	private Sucursal userAuthenticate() {
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		Sucursal sucursal = sucursalService
				.findByAdministrador(administradorService.findByUsuario(usuarioService.finByUsername(auth.getName())));
		return sucursal;
	}

	// Gestion de productos

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_products")
	public List<Producto> listProducts() {
		Sucursal sucursal = userAuthenticate();
		// System.out.println(p.getNombre());
		List<Producto> productos = productoService.findAllBySucursal(sucursal);
		if (productos.isEmpty()) {
			productos = null;
		}
		System.out.println("Todos los:" + productos);
		return productos;
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("list_products/page/{page}")
	public Page<Producto> listProducts(@PathVariable Integer page) {
		Sucursal sucursal = userAuthenticate();
		return productoService.findAllBySucursal(PageRequest.of(page, 4), sucursal);
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/mostrar_producto/{id}")
	public ResponseEntity<?> mostrarProducto(@PathVariable Long id) {
		Producto producto = null;
		ParametrosETLCSVModel productoActual = new ParametrosETLCSVModel();
		Map<String, Object> respuesta = new HashMap<>();
		try {
			producto = productoService.findById(id);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar la busqueda del registro con ID:" + id);
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (producto == null) {
			respuesta.put("mensaje", "El producto con ID:" + id + " no existe en la DB!");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/crear_producto")
	public ResponseEntity<?> crearProducto(@RequestBody Producto producto) {
		Producto productoNuevo = null;
		Producto productoCreado = null;
		Map<String, Object> respuesta = new HashMap<>();
		try {
			Sucursal sucursal = userAuthenticate();
			
			//SubCategoria sub = subCategoriaService.findById(producto.getSubcategoria().getSubcategoria_id());
			//producto.setSubcategoria(sub);
			producto.setSucursal(sucursal);
			producto.setEstado(true);
			System.out.println("ANTES DE GUARDAR");
			productoCreado = productoService.save(producto);
			System.out.println("Producto creado:" + productoCreado);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso del registro en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "El producto ha sido creado con exito!!");
		respuesta.put("producto", productoCreado);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@PutMapping("/modificar_producto/{id}")
	public ResponseEntity<?> modificarProducto(@RequestBody Producto producto, @PathVariable Long id) {
		Producto productoActual = productoService.findById(id);
		Producto productoModificar = null;
		Map<String, Object> respuesta = new HashMap<>();

		if (productoActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			Sucursal sucursal = userAuthenticate();
			productoActual.setNombre(producto.getNombre());
			productoActual.setDescripcion(producto.getDescripcion());
			productoActual.setCantidad(producto.getCantidad());
			productoActual.setPrecio(producto.getPrecio());
			productoActual.setEstado(producto.isEstado());
			productoActual.setSubcategoria(producto.getSubcategoria());
			productoActual.setSucursal(sucursal);

			productoModificar = productoService.save(productoActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al actualizar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El producto ha sido actualizado con exito!!");
		respuesta.put("producto", productoModificar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@DeleteMapping("/eliminar_producto/{id}")
	public ResponseEntity<?> eliminarProducto(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Producto productoActual = productoService.findById(id);
		Producto productoEliminar = null;
		if (productoActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			productoActual.setEstado(false);
			productoEliminar = productoService.save(productoActual);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al eliminar el registro en la DB con ID:" + id + " !!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "El producto ha sido eliminado con exito!!");
		respuesta.put("producto", productoEliminar);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}
	// Termina gestion de productos

	// Editar datos de sucursal por parte de cliente sucursal

	// Termina editar sucursal

	// Gestion categoria

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_categorias")
	public List<SubCategoria> listSubcategoria() {
		Sucursal sucursal = userAuthenticate();
		return subCategoriaService.findBySucursal(sucursal);
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/list_categorias/page/{page}")
	public Page<SubCategoria> listSubcategoria(@PathVariable Integer page) {
		Sucursal sucursal = userAuthenticate();
		return subCategoriaService.findBySucursal(sucursal,PageRequest.of(page, 4));
	}

	@Secured("ROLE_NEGOCIO")
	@GetMapping("/categoria/{id}")
	public ResponseEntity<?> getCategoria(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		SubCategoria subCategoria = null;
		try {
			subCategoria = subCategoriaService.findById(id);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el pedido de la categoria en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (subCategoria == null) {
			respuesta.put("mensaje", "La categoria con ID:" + id + " no existe en la DB!");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<SubCategoria>(subCategoria, HttpStatus.OK);
	}

	@Secured("ROLE_NEGOCIO")
	@PostMapping("/categoria")
	public ResponseEntity<?> crearCategoria(@RequestBody SubCategoria subCategoria) {
		SubCategoria subCategoriaNueva;
		Map<String, Object> respuesta = new HashMap<>();

		try {
			Sucursal sucursal = userAuthenticate();
			subCategoria.setEstado(true);
			subCategoria.setSucursal(sucursal);
			subCategoriaNueva = subCategoriaService.save(subCategoria);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de la categoria en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La categoria ha sido creado con exito!!");
		respuesta.put("categoria", subCategoriaNueva);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@PutMapping("/categoria/{id}")
	public ResponseEntity<?> modificarCategoria(@RequestBody SubCategoria subCategoria, @PathVariable Long id) {
		SubCategoria subCategoriActual, subCategoriaModificada;
		Map<String, Object> respuesta = new HashMap<>();

		subCategoriActual = subCategoriaService.findById(id);
		if (subCategoriActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			subCategoriActual.setNombre(subCategoria.getNombre());
			subCategoriaModificada = subCategoriaService.save(subCategoriActual);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar la actualización de la categoria en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La categoria ha sido modificado con exito!!");
		respuesta.put("categoria", subCategoriActual);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	@Secured("ROLE_NEGOCIO")
	@DeleteMapping("/categoria/{id}")
	public ResponseEntity<?> eliminarCategoria(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		SubCategoria subCategoriaActual, subCategoriaBloquear;
		subCategoriaActual = subCategoriaService.findById(id);
		if (subCategoriaActual == null) {
			respuesta.put("mensaje", "Error al obtener el registro con ID:" + id);
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		try {
			subCategoriaActual.setEstado(false);
			subCategoriaBloquear = subCategoriaService.save(subCategoriaActual);

		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al bloquear el registro en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		respuesta.put("mensaje", "La categoria ha sido bloqueado con exito!!");
		respuesta.put("categoria", subCategoriaBloquear);
		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	// termina gestion categoria

	// extraer linea de carrito por sucursal
	@Secured("ROLE_NEGOCIO")
	@GetMapping("/linea_carrito")
	public List<CartLine> lineasCarrito() {
		Sucursal sucursal = userAuthenticate();
		List<Producto> productos = productoService.findAllBySucursal(sucursal);
		List<CartLine> lineasCarrito = new ArrayList<CartLine>();
		for (Producto producto : productos) {
			System.out.println("lineas:"+cartLineService.findByProducto(producto));
			lineasCarrito.addAll(cartLineService.findByProducto(producto));
//			for(CartLine linea:cartLineService.findByProducto(producto)) {
//				lineasCarrito.add(linea);
//			}
		}
		return lineasCarrito;
	}

	// termina linea carrito

}
