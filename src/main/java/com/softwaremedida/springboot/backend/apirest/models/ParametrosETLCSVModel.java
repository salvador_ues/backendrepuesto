package com.softwaremedida.springboot.backend.apirest.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParametrosETLCSVModel {
	
	private ColumnasDBCentralModel columnas;
	private List<String> columasSeleccionadas;
	private List<List<String>> datosExcel;
	
	
	public ColumnasDBCentralModel getColumnas() {
		return columnas;
	}
	public void setColumnas(ColumnasDBCentralModel columnas) {
		this.columnas = columnas;
	}
	
	public List<String> getColumasSeleccionadas() {
		return columasSeleccionadas;
	}
	public void setColumasSeleccionadas(List<String> columasSeleccionadas) {
		this.columasSeleccionadas = columasSeleccionadas;
	}
	public List<List<String>> getDatosExcel() {
		return datosExcel;
	}
	public void setDatosExcel(List<List<String>> datosExcel) {
		this.datosExcel = datosExcel;
	}
	
}
