package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Comision;

@Repository("comsionRepository")
public interface ComisionRepository extends JpaRepository<Comision, Long>{
	
	@Query("from Comision c where c.estado= :estado")
	public Comision findComisionByEstado(@Param("estado")boolean estado);

}
