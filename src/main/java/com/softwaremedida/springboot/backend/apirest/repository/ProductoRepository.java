package com.softwaremedida.springboot.backend.apirest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Categoria;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long>{
	
	@Query("SELECT p FROM Producto p WHERE p.sucursal = ?1")
	public Page<Producto> findBySucursa(Sucursal sucursal, Pageable pageable);
	
	@Query("SELECT p FROM Producto p WHERE p.sucursal = ?1")
	public List<Producto> findBySucursa(Sucursal sucursal);
	
	@Query("from Producto p where p.estado= ?1")
	public List<Producto> findByEstado(boolean estado);
	
	@Query(value = "SELECT * FROM productos as p where p.estado = true and p.sucursal_id = (select s.sucursal_id from sucursal as s where s.estado = true)",nativeQuery = true)
	//@Query(value = "SELECT * FROM productos as p inner join sucursal as s on p.sucursal_id = s.sucursal_id where p.estado=true and s.estado=true ",nativeQuery = true)
	public List<Producto> findByEstado();
	
	@Query(value = "SELECT * FROM productos as p where p.estado = true and p.sucursal_id = (select s.sucursal_id from sucursal as s where s.estado = true)",nativeQuery = true)
	//@Query(value = "SELECT * FROM productos as p inner join sucursal as s on p.sucursal_id = s.sucursal_id where p.estado=true and s.estado=true ",nativeQuery = true)
	public Page<Producto> findByEstado(Pageable pageable);
	
//	@Query("SELECT p FROM Producto p WHERE p.sucursal=: sucursal and p.subcategoria = :subcategoria")
//	public List<Producto> findBySubCategoria(@Param("subcategoria") SubCategoria subcategoria);
	
	@Query("SELECT p FROM Producto p WHERE p.subcategoria = ?1 and p.estado = ?2")
	public List<Producto> findBySubCategoria(SubCategoria subcategoria,boolean estado);
	
	@Query("SELECT p FROM Producto p WHERE p.subcategoria = ?1 and p.estado = ?2")
	public Page<Producto> findBySubCategoria(SubCategoria subcategoria,boolean estado,Pageable pageable);
	
	@Query("SELECT p FROM Producto p WHERE p.nombre= ?1 and p.sucursal = ?2")
	public Producto findByNombreAndSucursal(String name,Sucursal sucursal);
	
}
