package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.repository.AdminRepository;

@Service
public class AdministradorServiceImp implements IAdministradorService{

	@Autowired
	private AdminRepository adminRepository;
	
	@Override
	public Administradores findById(Long id) {
		
		return adminRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Administradores> findAll() {
		
		return (List<Administradores>) adminRepository.findAll();
	}

	@Override
	public Administradores save(Administradores admin) {
		
		return adminRepository.save(admin);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Administradores> findAll(Pageable pageable) {
		
		return adminRepository.findAll(pageable);
	}

	@Override
	public Administradores findByUsuario(Usuario usuario) {
		
		return adminRepository.findByUsuario(usuario);
	}

	

}
