package com.softwaremedida.springboot.backend.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Cart;
import com.softwaremedida.springboot.backend.apirest.repository.CartRepository;

@Service("cartServiceImp")
public class CartServiceImp implements ICartService{

	
	@Autowired
	private CartRepository cartRepository;
	
	@Override
	public Cart save(Cart cart) {
		
		return cartRepository.save(cart);
	}
	

}
