package com.softwaremedida.springboot.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softwaremedida.springboot.backend.apirest.entity.Cart;
import com.softwaremedida.springboot.backend.apirest.entity.CartLine;
import com.softwaremedida.springboot.backend.apirest.entity.Categoria;
import com.softwaremedida.springboot.backend.apirest.entity.Comision;
import com.softwaremedida.springboot.backend.apirest.entity.Orden;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.models.CartLineModel;
import com.softwaremedida.springboot.backend.apirest.models.OrdenModel;
import com.softwaremedida.springboot.backend.apirest.models.ProductosFiltrosModel;
import com.softwaremedida.springboot.backend.apirest.models.TablaColumnaModel;
import com.softwaremedida.springboot.backend.apirest.repository.CategoriaRepository;
import com.softwaremedida.springboot.backend.apirest.services.IAdministradorService;
import com.softwaremedida.springboot.backend.apirest.services.ICartLineService;
import com.softwaremedida.springboot.backend.apirest.services.ICartService;
import com.softwaremedida.springboot.backend.apirest.services.IComisionService;
import com.softwaremedida.springboot.backend.apirest.services.IOrdenService;
import com.softwaremedida.springboot.backend.apirest.services.IProductoService;
import com.softwaremedida.springboot.backend.apirest.services.ISubCategoriaService;
import com.softwaremedida.springboot.backend.apirest.services.ISucursalService;
import com.softwaremedida.springboot.backend.apirest.services.UsuarioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api_cliente")
public class ClientRestController {
	
	ProductosFiltrosModel productosFiltroGlobal = new ProductosFiltrosModel();
	
	@Autowired
	private IProductoService productoService;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ISubCategoriaService subCategoriaService;
	
	@Autowired
	private ISucursalService sucursalService;
	
	@Autowired
	private IComisionService comisionService;
	
	@Autowired
	private ICartService cartService;
	
	@Autowired
	private ICartLineService cartLineService;
	
	@Autowired
	private IAdministradorService administradorService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private IOrdenService ordenService;
	
	private Usuario userAuthenticate() {
		Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
		
		return usuarioService.finByUsername(auth.getName());
	}
	
	@PostMapping("/list_productos_filtros")
	public String listProductos(@RequestBody ProductosFiltrosModel productosFiltros ) {
		productosFiltroGlobal.setNombreCategoria(productosFiltros.getNombreCategoria());
		productosFiltroGlobal.setNombreSucursal(productosFiltros.getNombreSucursal());
		System.out.println("OBJETOS:"+productosFiltros.getNombreCategoria()+" "+productosFiltros.getNombreSucursal());
		return null;
	}
	
//	@GetMapping("/list_productos")
//	public List<Producto> listProductos(){
//		List<Producto> producto = new ArrayList<Producto>();
//		
//		System.out.println(productosFiltroGlobal.getNombreCategoria()+" "+productosFiltroGlobal.getNombreSucursal());
//		
////		if(productosFiltroGlobal.getNombreCategoria()==" " && productosFiltroGlobal.getNombreSucursal()==" ") {
////			producto = productoService.findAll();
////		}
//		
//		if(productosFiltroGlobal.getNombreCategoria()=="TODOS" && productosFiltroGlobal.getNombreSucursal()=="TODOS") {
//			producto = productoService.findAll();
//			productosFiltroGlobal.setNombreCategoria(" ");
//			productosFiltroGlobal.setNombreSucursal(" ");
//		
//		} 
//		else if(productosFiltroGlobal.getNombreCategoria()=="TODOS") {
//			producto = productoService.findAllBySucursal(sucursalService.findById(Long.parseLong(productosFiltroGlobal.getNombreSucursal())));
//			productosFiltroGlobal.setNombreCategoria(" ");
//			productosFiltroGlobal.setNombreSucursal(" ");
//		}else if(productosFiltroGlobal.getNombreSucursal()=="TODOS") {
//			
//			producto = productoService.findAllBySubCategoria(subCategoriaService.findById(Long.parseLong(productosFiltroGlobal.getNombreCategoria())));
//			productosFiltroGlobal.setNombreCategoria(" ");
//			productosFiltroGlobal.setNombreSucursal(" ");
//		}else {
//			producto = productoService.findAll();
//		}
//		
//		return producto;
//	}
	
	@Secured("ROLE_USER")
	@GetMapping("list_productos/{id}")
	public List<Producto> listProductos(@PathVariable String id){
		List<Producto> producto =null;
		System.out.println("IMPRESION AFUERA:"+id);
		if((id.isEmpty() || "todo".equals(id))) {
			producto= productoService.findAll();
		}else{
			Long id2 = Long.parseLong(id);
			producto = productoService.findAllBySubCategoria(subCategoriaService.findById(id2));
		}
		return producto;
	}
		
	@GetMapping("/list_subcategorias")
	public List<SubCategoria> listCategorias(){
		
		return (List<SubCategoria>) subCategoriaService.findAll();
	}
	
//	@GetMapping("/list_sucursales")
//	public List<Sucursal> listSucursales(){
//		
//		return sucursalService.findAll();
//	}	
	
	@Secured("ROLE_USER")
	@PostMapping("/order")
	public ResponseEntity<?> crearOrden(@RequestBody OrdenModel orden){
		Map<String, Object> respuesta = new HashMap<>();
		Orden ordenNueva=null;
		Cart carritoNuevo = null;
		CartLine lineaCarrito = null;
		
		Comision comision= comisionService.findComisionByEstado(true);
		if(comision==null) {
			respuesta.put("mensaje", "Error al obtener el registro con");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}
		try {
			
			double montoComision = orden.getCart().getPrecioCart()*(comision.getComision()/100);
			
			double montoComisionLinea=0,gananciaNetaLinea=0;
			
			carritoNuevo = new Cart();
			carritoNuevo.setCantidadItem(orden.getCart().getCantidadItem());
			carritoNuevo.setPrecioCart(orden.getCart().getPrecioCart());
			carritoNuevo.setComision(comision.getComision());
			carritoNuevo.setMontoComision(montoComision);
			carritoNuevo = cartService.save(carritoNuevo);
			
			ordenNueva = new Orden();
			ordenNueva.setNombre_remitente(orden.getNombre_remitente());
			ordenNueva.setDireccion(orden.getDireccion());
			ordenNueva.setCiudad(orden.getCiudad());
			ordenNueva.setEnviado(orden.isEnviado());
			ordenNueva.setCart(carritoNuevo);
			ordenNueva.setUsuario(userAuthenticate());
			ordenNueva = ordenService.save(ordenNueva);
			
			Producto productodescontar=null;
			for (CartLineModel linea : orden.getCart().getLineas()) {
			//	System.out.println("linea- Producto:"+linea.getProducto().getProducto_id()+" "+linea.getProducto().getNombre()+" Cantidad:"+linea.getCantidad());
				montoComisionLinea=(linea.getProducto().getPrecio()*linea.getCantidad())*(comision.getComision()/100);
				gananciaNetaLinea = (linea.getProducto().getPrecio()*linea.getCantidad())-montoComisionLinea;
				
				lineaCarrito = new CartLine();
				lineaCarrito.setCart(carritoNuevo);
				lineaCarrito.setProducto(linea.getProducto());
				lineaCarrito.setCantidad(linea.getCantidad());
				lineaCarrito.setLineaTotal(linea.getProducto().getPrecio()*linea.getCantidad());
				lineaCarrito.setComision(comision.getComision()/100);
				lineaCarrito.setComisionMonto(montoComisionLinea);
				lineaCarrito.setGananciaNeta(gananciaNetaLinea);
				cartLineService.save(lineaCarrito);
				
				montoComisionLinea=gananciaNetaLinea=0;
				
				productodescontar = productoService.findById(linea.getProducto().getProducto_id());
				productodescontar.setCantidad(productodescontar.getCantidad()-linea.getCantidad());
				productoService.save(productodescontar);
			}
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al realizar el ingreso de la orden en la DB!!");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(respuesta,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		respuesta.put("mensaje", "La orden ha sido creada con exito!!");
		respuesta.put("orden", ordenNueva);
		return new ResponseEntity<Map<String,Object>>(respuesta,HttpStatus.CREATED);
	}
	
	@Secured("ROLE_USER")
	@GetMapping("/list_ordenes/page/{page}")
	public Page<Orden> listOrdentes(@PathVariable Integer page){
		
		
		return ordenService.findAll(userAuthenticate(),PageRequest.of(page, 5));
	}
	
}
