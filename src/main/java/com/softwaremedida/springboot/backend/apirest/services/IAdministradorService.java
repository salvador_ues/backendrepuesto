package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

public interface IAdministradorService {
	
	public abstract Administradores findById(Long id);
	public abstract List<Administradores> findAll();
	public abstract Page<Administradores> findAll(Pageable pageable);
	public abstract Administradores save(Administradores admin);
	public abstract Administradores findByUsuario(Usuario usuario);
}
