package com.softwaremedida.springboot.backend.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Orden;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.repository.OrdenRepository;

@Service("ordenService")
public class OrdenServiceImp implements IOrdenService {

	@Autowired
	private OrdenRepository ordenRepository;
	
	@Override
	public Orden save(Orden orden) {
		
		return ordenRepository.save(orden);
	}

	@Override
	public Page<Orden> findAll(Usuario usuario,Pageable pageable) {
		
		return ordenRepository.findByUsuario(usuario,pageable);
	}

	@Override
	public Orden findOrdenById(Long id) {
		
		return ordenRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Orden> findAll(Pageable pageable) {
		
		return ordenRepository.findAll(pageable);
	}

}
