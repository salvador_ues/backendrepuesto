package com.softwaremedida.springboot.backend.apirest.models;

import java.util.List;

public class ProductosFiltrosModel {
	
	private String nombreCategoria;
	
	private String nombreSucursal;

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}

	public ProductosFiltrosModel(String nombreCategoria, String nombreSucursal) {
		super();
		this.nombreCategoria = nombreCategoria;
		this.nombreSucursal = nombreSucursal;
	}
	
	public ProductosFiltrosModel(){}	
	

}
