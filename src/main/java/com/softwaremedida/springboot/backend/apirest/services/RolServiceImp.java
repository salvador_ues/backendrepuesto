package com.softwaremedida.springboot.backend.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Rol;
import com.softwaremedida.springboot.backend.apirest.repository.RoleRepository;

@Service
public class RolServiceImp implements IRolService {

	@Autowired
	private RoleRepository rolRepository;
	
	@Override
	public Rol findByNombre(String nombre) {
		
		return rolRepository.findByNombre(nombre);
	}

	@Override
	public void save(Rol rol) {
		rolRepository.save(rol);
		
	}
	
}
