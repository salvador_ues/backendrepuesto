package com.softwaremedida.springboot.backend.apirest.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.ConsultasDB;

@Repository("consultaRepository")
public interface ConsultasRepository extends JpaRepository<ConsultasDB, Long>{
	
	@Query("from ConsultasDB cd where cd.conexion= ?1")
	public Page<ConsultasDB> findByConexion(Conexion conexion,Pageable pageable);
	
	@Query("from ConsultasDB cd where cd.conexion = ?1")
	public List<ConsultasDB> findAllByConexion(Conexion conexion);
 
}
