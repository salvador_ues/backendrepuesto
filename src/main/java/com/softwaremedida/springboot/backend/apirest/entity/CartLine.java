package com.softwaremedida.springboot.backend.apirest.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="linea_carro")
public class CartLine extends AuditingModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long linea_carro_id;
	
	@NotNull(message="El producto no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "producto_id", nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Producto producto;
	
	private Integer cantidad;
	
	private Double lineaTotal = 0D;
	
	private double comision=0;
	
	private double comisionMonto=0;
	
	private double gananciaNeta=0;
	
	@NotNull(message="El carrito no puede ser vacio")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "carro_id", nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Cart cart;

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	} 

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getLineaTotal() {
		return lineaTotal;
	}

	public void setLineaTotal(Double lineaTotal) {
		this.lineaTotal = lineaTotal;
	}

	public Long getLinea_carro_id() {
		return linea_carro_id;
	}

	public void setLinea_carro_id(Long linea_carro_id) {
		this.linea_carro_id = linea_carro_id;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public double getComisionMonto() {
		return comisionMonto;
	}

	public void setComisionMonto(double comisionMonto) {
		this.comisionMonto = comisionMonto;
	}

	public double getGananciaNeta() {
		return gananciaNeta;
	}

	public void setGananciaNeta(double gananciaNeta) {
		this.gananciaNeta = gananciaNeta;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}
	
	

}
