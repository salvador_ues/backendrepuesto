package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Comision;

public interface IComisionService {
	
	public List<Comision> findAll();
	
	public Page<Comision> findAll(Pageable pageable);
	
	public Comision add(Comision comision);
	
	public void delete(Long id);
	
	public Comision findById(Long id);
	
	public Comision findComisionByEstado(boolean estado);

}
