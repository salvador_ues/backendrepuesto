package com.softwaremedida.springboot.backend.apirest.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="consultas_db")
public class ConsultasDB extends AuditingModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long consulta_id;
	
	@NotEmpty(message = "no puede estar vacio")
	@Column(length = 500)
	private String consulta;
	
	@NotNull(message="la conexión no puede ser vacia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="conexion_id",nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private  Conexion conexion;
	
	private String tipoconsulta;
	
	@OneToOne
    @JoinColumn(name = "relacion_id", referencedColumnName = "id")
	private RelacionColumna relacion;

	public Long getConsulta_id() {
		return consulta_id;
	}

	public void setConsulta_id(Long consulta_id) {
		this.consulta_id = consulta_id;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public Conexion getConexion() {
		return conexion;
	}

	public void setConexion(Conexion conexion) {
		this.conexion = conexion;
	}

	public String getTipoconsulta() {
		return tipoconsulta;
	}

	public void setTipoconsulta(String tipoconsulta) {
		this.tipoconsulta = tipoconsulta;
	}

	public RelacionColumna getRelacion() {
		return relacion;
	}

	public void setRelacion(RelacionColumna relacion) {
		this.relacion = relacion;
	}

	
	
}
