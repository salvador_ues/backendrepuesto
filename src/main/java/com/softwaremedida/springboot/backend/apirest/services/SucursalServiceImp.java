package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.repository.SucursalRepository;

@Service
public class SucursalServiceImp implements ISucursalService{
	
	@Autowired
	private SucursalRepository sucursalRepository;
	
	@Override
	public Sucursal findById(Long id) {
		
		return sucursalRepository.findById(id).orElse(null);
	}

	@Override
	public List<Sucursal> findAll() {
		
		return (List<Sucursal>) sucursalRepository.findAll();
	}

	@Override
	public Sucursal saveSucursal(Sucursal sucursal) {
		
		return sucursalRepository.save(sucursal);
	}

	@Override
	public Page<Sucursal> findAll(Pageable pageable) {
	
		return sucursalRepository.findAll(pageable);
	}

	@Override
	public Sucursal findByAdministrador(Administradores admin) {
		
		return sucursalRepository.findByAdministrador(admin);
	}

}
