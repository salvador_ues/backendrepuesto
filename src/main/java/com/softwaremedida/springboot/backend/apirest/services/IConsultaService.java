package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.ConsultasDB;

public interface IConsultaService {
	
	public abstract ConsultasDB save(ConsultasDB consulta);
	public abstract ConsultasDB findById(Long id);
	public abstract Page<ConsultasDB> findAllByConexion(Conexion conexion,Pageable pageable);
	
	public abstract List<ConsultasDB> findAllByConexion(Conexion conexion);

}
