package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="conexiones")
public class Conexion extends AuditingModel{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long conexion_id;
	
	@NotEmpty(message = "no puede estar vacio")
	private String gestor;
	
	@NotEmpty(message = "no puede estar vacio")
	private String nombre;
	
	@NotEmpty(message = "no puede estar vacio")
	private String host;
	
	@NotEmpty(message = "no puede estar vacio")
	private String puerto;
	
	@NotEmpty(message = "no puede estar vacio")
	private String usuario;
	
	private String password;
	
	private boolean estado;
	
	@NotNull(message="La sucursal no puede ser vacio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sucursal_id",nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private  Sucursal sucursal;
	
//	@OneToMany(mappedBy = "conexion")
//	private Set<ConsultasDB> consultasDB;
	
 
	public Long getConexion_id() {
		return conexion_id;
	}

	public void setConexion_id(Long conexion_id) {
		this.conexion_id = conexion_id;
	}
	
	public String getGestor() {
		return gestor;
	}
	
	public String getNombre() {
		return nombre;
	}
	public String getHost() {
		return host;
	}
	public String getPuerto() {
		return puerto;
	}
	public String getUsuario() {
		return usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setGestor(String gestor) {
		this.gestor = gestor;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	//	public Set<ConsultasDB> getConsultasDB() {
//		return consultasDB;
//	}
//	public void setConsultasDB(Set<ConsultasDB> consultasDB) {
//		this.consultasDB = consultasDB;
//	}
	public Conexion(){}

	public Conexion(@NotEmpty(message = "no puede estar vacio") String gestor,
			@NotEmpty(message = "no puede estar vacio") String nombre,
			@NotEmpty(message = "no puede estar vacio") String host,
			@NotEmpty(message = "no puede estar vacio") String puerto,
			@NotEmpty(message = "no puede estar vacio") String usuario, String password, boolean estado,
			@NotNull(message = "La sucursal no puede ser vacio") Sucursal sucursal) {
		super();
		this.gestor = gestor;
		this.nombre = nombre;
		this.host = host;
		this.puerto = puerto;
		this.usuario = usuario;
		this.password = password;
		this.estado = estado;
		this.sucursal = sucursal;
	}
	
	
	
	
	
}
