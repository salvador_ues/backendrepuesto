package com.softwaremedida.springboot.backend.apirest.auth;

public class JwtConfig {
	public static final String LLAVE_PRIVADA="\r\n" + 
			"-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEogIBAAKCAQEAyryNO8Yxh7xjjODXD1RrXNRDkV50G7DUHj3JcpKoNhnrWuYf\r\n" + 
			"w4remFQ2H+lMIJ41oqAJ2FzNUFAsnm5xn7fhX9LW67gIXPrQSmhjvbAOGcfcDrWT\r\n" + 
			"yLlCZQsBqrjVz9dEIyNOu50O8J56n+WN1Nyl41hjuSnMR2m5e0qeg/LKY3csQyUz\r\n" + 
			"hRJiPb3vDDm3n9Vc28B9skvQl87vVxoxxkhpnnr8hxmxTD7jMPFrt/Yr8bc6HKcB\r\n" + 
			"znbgpgxKQmyYeUeh0M8R8RHtDuspU2EEMq91UuOaKBpOVYbscbKyLWNFlCBqy6mi\r\n" + 
			"BjFaWHyq8TC/+TYM/IeTKLRXl7/838clLehk0wIDAQABAoIBAGesqGTK44VQN3BR\r\n" + 
			"gmjaeNTNz4fFT4bhlHeqjRvEF7Ui6BJh4Bh1KYOofM0w1rdPcIorYR19x5ish0S1\r\n" + 
			"ofFVbTE9dp6wS8aQlHeYjnjwUSWp6MS04lAhPOzDBiUxxFU29icp5RdYJ82wnXCr\r\n" + 
			"SS5zb3mlN8n2r/pM/IcqD4nWXN0jqTn8LzFRcU70ZU01GVdZQa+yh/iTKThe3v3U\r\n" + 
			"rqgZ2AhJtOXCXShr7snpq6ifc9Y850eWgCIOjfDblvWt6eNYZEnPtH0zz3Uu7GWK\r\n" + 
			"x2lPFFGQeg35BJHlOWUjRZd0qz0hMTRRogB1IBBWiUKz4jLPw+WjYfvHwdDMTOU+\r\n" + 
			"mri01bECgYEA6AiQyTTx1Y7VJghyZKVPV4tN2hGk8ZTlvATvPm44cKq6lWtygWFT\r\n" + 
			"G+5Bt7ZdGbDPWMMdlKWNZYD0ftVOwqRdgHmyD+vMBgHsyT9IYqxsKOkAWtPkI/B9\r\n" + 
			"453LvZx9Bc4mujxCAdOUIMl58VEAjzVj6EnyAg3UxrURd5kKAoQ1H5UCgYEA361Q\r\n" + 
			"5XdekfdTU5ETCKLneztNcRUGXtbZzlFr84lflLerGOh66CeX/SzGfBbmsPSXLK2I\r\n" + 
			"VB3BIkwVfTpUFoF358oev0hp/11Nj2tCSNrdP+l2I/q1plD6V96DxSurs5tJnhAv\r\n" + 
			"Zzp1QXYHIMSVxudm2J8oOxXhMDzYy4JS3hLTeMcCgYBw3vnPZrndMMXu4l0DKIcY\r\n" + 
			"BuOwGLUr70px09StgeBiriplVQc2eKl9BMFjJIrVXHUAsKlLDd1iawCEUi8l796s\r\n" + 
			"CIbdx85PfdA5dVc46LcUj4Nkq35nHS2f87qQWfdXQ5kDLxzM1qdP67It+UFUaJxY\r\n" + 
			"+t+GsEO5BYuhqgh8gON2oQKBgA6aq1iAJb2UD226YCrFMCX8pj7G56aTkp8E8+5v\r\n" + 
			"vrzgDRzQOQ4lX6gZH1CE0kUlgyVYozkSxERwAUsu3sSlaWXJ7Z4BpWIV/nVpxGcr\r\n" + 
			"WxzMKnmI6glU6jOIhMP/6PMOJY1bcyzjRTqj8S0ziK/29eEY6zCIai6TGniXJr/3\r\n" + 
			"LLWLAoGAa4QHUgfaR5O+yOtQTEuRZKmMyLjn9TZ16nBioUMs/XD+7tigR1l9ET2y\r\n" + 
			"Blhh5+erh16NBmaEh0BCa0NKT+g+hLEwqbwxeepUj/X0HW7wXPmgc/5gU+taID6b\r\n" + 
			"bJMnF3vC7fiG6s1ufTBN+GaEkS2V398nTQzfFa3W6RuOh5FMwC4=\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String LLAVE_PUBLICA = "\r\n" + 
			"-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyryNO8Yxh7xjjODXD1Rr\r\n" + 
			"XNRDkV50G7DUHj3JcpKoNhnrWuYfw4remFQ2H+lMIJ41oqAJ2FzNUFAsnm5xn7fh\r\n" + 
			"X9LW67gIXPrQSmhjvbAOGcfcDrWTyLlCZQsBqrjVz9dEIyNOu50O8J56n+WN1Nyl\r\n" + 
			"41hjuSnMR2m5e0qeg/LKY3csQyUzhRJiPb3vDDm3n9Vc28B9skvQl87vVxoxxkhp\r\n" + 
			"nnr8hxmxTD7jMPFrt/Yr8bc6HKcBznbgpgxKQmyYeUeh0M8R8RHtDuspU2EEMq91\r\n" + 
			"UuOaKBpOVYbscbKyLWNFlCBqy6miBjFaWHyq8TC/+TYM/IeTKLRXl7/838clLehk\r\n" + 
			"0wIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
}
