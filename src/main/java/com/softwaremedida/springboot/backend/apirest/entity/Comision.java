package com.softwaremedida.springboot.backend.apirest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="comision")
public class Comision extends AuditingModel{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long comision_id;
	
	@NotNull(message = "no puede estar vacio")
	@Column(precision = 6, scale=2,nullable = false)
	private double comision;
	
	private boolean estado;
	
	@NotNull(message="el admin no puede ser vacia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="administradorTotal_id",nullable = false)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private AdministradorTotal administradorTotal;

	

	public Long getComision_id() {
		return comision_id;
	}

	public void setComision_id(Long comision_id) {
		this.comision_id = comision_id;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	
	public AdministradorTotal getAdministradorTotal() {
		return administradorTotal;
	}

	public void setAdministradorTotal(AdministradorTotal administradorTotal) {
		this.administradorTotal = administradorTotal;
	}
	
	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Comision() {
		
	}

	public Comision(@NotNull(message = "no puede estar vacio") double comision, boolean estado,
			@NotNull(message = "la región no puede ser vacia") AdministradorTotal administradorTotal) {
		super();
		this.comision = comision;
		this.estado = estado;
		this.administradorTotal = administradorTotal;
	}

	


	
}
