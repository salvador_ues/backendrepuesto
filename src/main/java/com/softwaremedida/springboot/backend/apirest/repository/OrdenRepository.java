package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Orden;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
@Repository("ordenRepository")
public interface OrdenRepository extends JpaRepository<Orden, Long>{
	
	public Page<Orden> findByUsuario(Usuario usuario,Pageable pageable);

}
