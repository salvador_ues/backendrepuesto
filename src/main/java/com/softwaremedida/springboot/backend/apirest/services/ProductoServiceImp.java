package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.repository.ProductoRepository;

@Service
public class ProductoServiceImp implements IProductoService{

	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public List<Producto> findAllBySucursal(Sucursal sucursal) {
		// TODO Auto-generated method stub
		return productoRepository.findBySucursa(sucursal);
	}

	@Override
	public List<Producto> findAll() {
		
		return (List<Producto>) productoRepository.findByEstado(true);
	}

	@Override
	public List<Producto> findAllBySubCategoria(SubCategoria subCategoria) {
		
		return productoRepository.findBySubCategoria(subCategoria,true);
	}

	@Override
	public Producto save(Producto producto) {
		
		return productoRepository.save(producto);
	}

	@Override
	public Producto findById(Long id) {
		return productoRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Producto> findAllBySucursal(Pageable pageable, Sucursal sucursal) {
		
		return (Page<Producto>) productoRepository.findBySucursa(sucursal,pageable);
	}

	@Override
	public Producto findByNombreAndSucursal(String nombre, Sucursal sucursal) {
		
		return productoRepository.findByNombreAndSucursal(nombre, sucursal);
	}

	@Override
	public Page<Producto> findAll(Pageable pageable) {
	
		return productoRepository.findByEstado(pageable);
	}

	@Override
	public Page<Producto> findAllBySubCategoria(SubCategoria subCategoria, Pageable page) {
		
		return productoRepository.findBySubCategoria(subCategoria, true, page);
	}

}
