package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.CartLine;
import com.softwaremedida.springboot.backend.apirest.entity.Producto;
import com.softwaremedida.springboot.backend.apirest.repository.CartLineRepository;
import com.softwaremedida.springboot.backend.apirest.repository.OrdenRepository;

@Service("cartLineServiceImp")
public class CartLineServiceImp implements ICartLineService{

	@Autowired
	private CartLineRepository cartLineRepository;
	
	@Override
	public CartLine save(CartLine cartLine) {
		
		return cartLineRepository.save(cartLine);
	}

	@Override
	public List<CartLine> findByProducto(Producto producto) {
		
		return cartLineRepository.findByProducto(producto);
	}

}
