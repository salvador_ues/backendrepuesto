package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.repository.CrudRepository;

import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	public Usuario findByUsername(String username);
}
