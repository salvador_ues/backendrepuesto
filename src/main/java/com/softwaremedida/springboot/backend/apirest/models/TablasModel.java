package com.softwaremedida.springboot.backend.apirest.models;

public class TablasModel {
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TablasModel(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TablasModel(){}
	@Override
	public String toString() {
		return "Tablas [id=" + id + ", name=" + name + "]";
	}
	
	

}
