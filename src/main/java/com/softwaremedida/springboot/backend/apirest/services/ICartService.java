package com.softwaremedida.springboot.backend.apirest.services;

import com.softwaremedida.springboot.backend.apirest.entity.Cart;

public interface ICartService {
	
	public abstract Cart save(Cart cart);

}
