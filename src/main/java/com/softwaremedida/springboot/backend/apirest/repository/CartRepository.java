package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Cart;

@Repository("cartRepository")
public interface CartRepository extends JpaRepository<Cart, Long> {

}
