package com.softwaremedida.springboot.backend.apirest.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="sucursal")
public class Sucursal extends AuditingModel implements Serializable{
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sucursal_id_seq")
//	@SequenceGenerator(
//		      name = "sucursal_id_seq", 
//		      sequenceName = "sucursal_seq", 
//		      allocationSize = 50
//		  )
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sucursal_generator")
//	@SequenceGenerator(name="sucursal_generator", sequenceName = "sucursal_seq")
//	@Column(name = "id_sucursal", updatable = false, nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long sucursal_id;
	
	@NotEmpty(message = "no puede estar vacio")
	private String nombre;
	
	@Email(message="no es una dirección de correo bien formada")
	@Column(unique=true)
	private String email;
	
	private boolean estado;
//	
//	@OneToMany(mappedBy = "sucursal")
//	private Set<ClienteSucursal> clienteSucursal;
	
	@NotNull(message = "no puede estar vacio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="administrador_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private  Administradores administrador;

	

	public Long getSucursal_id() {
		return sucursal_id;
	}

	public void setSucursal_id(Long sucursal_id) {
		this.sucursal_id = sucursal_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Administradores getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Administradores administrador) {
		this.administrador = administrador;
	}
	
	private static final long serialVersionUID = 1L;

	public Sucursal( @NotEmpty String nombre, @NotEmpty String email, boolean estado,
			Administradores administrador) {
		super();
		this.nombre = nombre;
		this.email = email;
		this.estado = estado;
		this.administrador = administrador;
	}
	
	public Sucursal() {}

	
	
	
	
	
}
