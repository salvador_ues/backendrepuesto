package com.softwaremedida.springboot.backend.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.RelacionColumna;
import com.softwaremedida.springboot.backend.apirest.repository.RelacionColumnaRepository;

@Service
public class RelacionColumnaImp implements IRelacionColumnaService{

	@Autowired
	private RelacionColumnaRepository relacionRepository;
	
	@Override
	public RelacionColumna findById(Long id) {
		
		return relacionRepository.findById(id).orElse(null);
	}

	@Override
	public RelacionColumna save(RelacionColumna relacion) {
		
		return relacionRepository.save(relacion);
	}

}
