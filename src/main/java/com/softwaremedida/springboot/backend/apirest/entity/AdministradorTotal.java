package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="administrador_total")
public class AdministradorTotal extends AuditingModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long administradorTotal_id;
	
	@NotEmpty(message = "no puede estar vacio")
	private String codigo;
	
//	@OneToMany(mappedBy = "administradorTotal")
//	private Set<Comision> comision;

	public Long getAdministradorTotal_id() {
		return administradorTotal_id;
	}

	public void setAdministradorTotal_id(Long administradorTotal_id) {
		this.administradorTotal_id = administradorTotal_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public AdministradorTotal( @NotEmpty(message = "no puede estar vacio") String codigo) {
		super();	
		this.codigo = codigo;
	}

	public AdministradorTotal() {
		super();
	}
	
	

//	public Set<Comision> getComision() {
//		return comision;
//	}
//
//	public void setComision(Set<Comision> comision) {
//		this.comision = comision;
//	}
//	
	
	
	
}
