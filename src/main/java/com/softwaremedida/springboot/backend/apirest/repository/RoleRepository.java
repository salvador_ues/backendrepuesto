package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Rol;

@Repository
public interface RoleRepository extends JpaRepository<Rol, Long>{
	
	public Rol findByNombre(String nombre);

}
