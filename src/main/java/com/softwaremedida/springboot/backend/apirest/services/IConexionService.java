package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

public interface IConexionService {
	
	public abstract Conexion save(Conexion conexion);
	public abstract Conexion getConexion(Long id);
	public abstract List<Conexion> findAll(Sucursal sucursal);
	public abstract Page<Conexion> findAll(Sucursal sucursal, Pageable pageable);
 
}
