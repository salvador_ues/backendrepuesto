package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

@Repository
public interface AdminRepository extends JpaRepository<Administradores,Long>{
	
	public abstract Administradores findByUsuario(Usuario usuario);

}
