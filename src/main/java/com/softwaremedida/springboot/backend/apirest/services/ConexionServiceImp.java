package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Conexion;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;
import com.softwaremedida.springboot.backend.apirest.repository.ConexionRepository;

@Service("conexionServiceImp")
public class ConexionServiceImp implements IConexionService {
	
	@Autowired
	@Qualifier("conexionRepository")
	private ConexionRepository conexionRepository;

	@Override
	public Conexion save(Conexion conexion) {
		
		return conexionRepository.save(conexion);
	}


	@Override
	public Conexion getConexion(Long id) {
		// TODO Auto-generated method stub
		return conexionRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Conexion> findAll(Sucursal sucursal) {
		
		return (List<Conexion>) conexionRepository.findAllByEstado(sucursal);
	}


	@Override
	@Transactional(readOnly = true)
	public Page<Conexion> findAll(Sucursal sucursal, Pageable pageable) {
		
		return (Page<Conexion>) conexionRepository.findAllByEstado(sucursal, pageable);
	}

}
