package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Administradores;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

public interface ISucursalService {
	
	public abstract Sucursal findById(Long id);
	public abstract List<Sucursal> findAll();
	public abstract Page<Sucursal> findAll(Pageable pageable);
	public abstract Sucursal saveSucursal(Sucursal sucursal);
	public abstract Sucursal findByAdministrador(Administradores admin);

}
