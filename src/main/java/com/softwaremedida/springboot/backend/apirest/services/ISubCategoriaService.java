package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.SubCategoria;
import com.softwaremedida.springboot.backend.apirest.entity.Sucursal;

public interface ISubCategoriaService {
	
	public abstract List<SubCategoria> findBySucursal(Sucursal sucursal);
	public abstract Page<SubCategoria> findBySucursal(Sucursal sucursal,Pageable pageable);
	public abstract SubCategoria findById(Long id);
	public abstract SubCategoria save(SubCategoria nombre);
	public abstract SubCategoria findByNombre(String nombre);
	public abstract List<SubCategoria> findAll();
	public abstract SubCategoria findByNombreAndSucursal(String nombre, Sucursal sucursal);
}
