package com.softwaremedida.springboot.backend.apirest.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.softwaremedida.springboot.backend.apirest.entity.Orden;
import com.softwaremedida.springboot.backend.apirest.entity.Usuario;

public interface IOrdenService {

	public abstract Orden save(Orden orden);
	public abstract Page<Orden> findAll(Usuario usuario,Pageable pageable);
	public abstract Orden findOrdenById(Long id);
	public abstract Page<Orden> findAll(Pageable pageable);
}
