package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.AdministradorTotal;

@Repository
public interface AdminTotalRepository extends CrudRepository<AdministradorTotal, Long>{

	public AdministradorTotal findByCodigo(String codigo);
}
