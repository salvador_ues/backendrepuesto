package com.softwaremedida.springboot.backend.apirest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.softwaremedida.springboot.backend.apirest.entity.RelacionColumna;

@Repository
public interface RelacionColumnaRepository extends CrudRepository<RelacionColumna, Long> {

}
