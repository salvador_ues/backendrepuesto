package com.softwaremedida.springboot.backend.apirest.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="sub_categoria")
public class SubCategoria extends AuditingModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long subcategoria_id;
	
	@NotEmpty(message = "no puede estar vacio")
	private String nombre;
	
	private boolean estado;
//	
	@NotNull(message="La sucursal no puede ser vacia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sucursal_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Sucursal sucursal;
	
	public String getNombre() {
		return nombre;
	}

	public Long getSubcategoria_id() {
		return subcategoria_id;
	}

	public void setSubcategoria_id(Long subcategoria_id) {
		this.subcategoria_id = subcategoria_id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public SubCategoria() {
		super();
	}
	
	public SubCategoria(@NotEmpty String nombre,boolean estado) {
		super();
		this.nombre = nombre;
		this.estado = estado;
	}
	
	
}
