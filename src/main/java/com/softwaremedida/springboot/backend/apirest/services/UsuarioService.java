package com.softwaremedida.springboot.backend.apirest.services;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softwaremedida.springboot.backend.apirest.entity.Usuario;
import com.softwaremedida.springboot.backend.apirest.repository.UsuarioRepository;

import ch.qos.logback.classic.Logger;

@Service("userService")
public class UsuarioService implements IUsuarioService, UserDetailsService {
	
	private org.slf4j.Logger logger = LoggerFactory.getLogger(UsuarioService.class);
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByUsername(username);
		if(usuario==null) {
			logger.error("Error en el login, no existe el usuario"+username);
			throw new UsernameNotFoundException("Error en el login, no existe el usuario"+username);
		}
		
		List<GrantedAuthority> authorities = usuario.getRoles().stream()
				.map(role->new SimpleGrantedAuthority(role.getNombre()))
				.peek(authority->logger.info("Role:"+authority.getAuthority()))
				.collect(Collectors.toList());
		
		return new User(usuario.getUsername(), usuario.getPassword(), usuario.getEnabled(), true, true, true, authorities);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario finByUsername(String username) {
		
		return usuarioRepository.findByUsername(username);
	}

	@Override
	public Usuario save(Usuario usuario) {
		
		return usuarioRepository.save(usuario);
	}

	@Override
	public Usuario findById(Long id) {
		
		return usuarioRepository.findById(id).orElse(null);
	}

}
